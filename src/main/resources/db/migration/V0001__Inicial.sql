CREATE TABLE gestop.perfil (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	nome varchar(255) NOT NULL,
	CONSTRAINT perfil_pkey PRIMARY KEY (id)
);

CREATE TABLE gestop.permissao (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	descricao varchar(255) NOT NULL,
	CONSTRAINT permissao_pkey PRIMARY KEY (id),
	CONSTRAINT permissao_nome UNIQUE (descricao)
);

CREATE TABLE gestop.perfil_permissao (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	perfil_id int8 NULL,
	permissao_id int8 NULL,
	CONSTRAINT perfil_permissao_pkey PRIMARY KEY (id),
	CONSTRAINT permissa_perfil_perfil_fk FOREIGN KEY (perfil_id) REFERENCES gestop.perfil(id),
	CONSTRAINT permissa_perfil_permissao_fk FOREIGN KEY (permissao_id) REFERENCES gestop.permissao(id)
);

CREATE TABLE gestop.usuario (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	cpf varchar(14) NULL,
	email varchar(60) NOT NULL,
	identificador varchar(7) NOT NULL,
	login varchar(30) NOT NULL,
	nome varchar(60) NOT NULL,
	tentativa_login int4 NULL DEFAULT 0,
	senha varchar(255) NOT NULL,
	setor varchar(60) NOT NULL,
	super_administrador bool NOT NULL,
	perfil_id int8 NOT NULL,
	ultima_tentativa_login timestamp NULL,
	data_nascimento date NULL,
	CONSTRAINT usuario_email_unique UNIQUE (email),
	CONSTRAINT usuario_login_unique UNIQUE (login),
	CONSTRAINT usuario_pkey PRIMARY KEY (id),
	CONSTRAINT usuario_perfil_fk FOREIGN KEY (perfil_id) REFERENCES gestop.perfil(id)
);

--EMAIL
CREATE TABLE gestop.email (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	criptografado bool NOT NULL,
	email_from varchar(120) NOT NULL,
	email_from_senha varchar(20) NOT NULL,
	email_from_usuario varchar(120) NOT NULL,
	porta_smtp int4 NOT NULL,
	servidor_smtp varchar(40) NOT NULL,
	CONSTRAINT email_pkey PRIMARY KEY (id),
	CONSTRAINT email_serv_port_mail_uk UNIQUE (servidor_smtp, porta_smtp, email_from)
);

CREATE TABLE gestop.parametro_sistema (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	descricao varchar(255) NULL,
	nome varchar(255) NULL,
	valor varchar(255) NULL,
	CONSTRAINT ctr_param_sis_nm_uk UNIQUE (nome),
	CONSTRAINT parametro_sistema_pkey PRIMARY KEY (id),
	CONSTRAINT parametro_sistema_nome_uk UNIQUE (nome)
);

CREATE TABLE gestop.estado (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	nome varchar(60) NOT NULL,
	sigla varchar(2) NOT NULL,
	CONSTRAINT estado_nome_unique UNIQUE (nome),
	CONSTRAINT estado_pkey PRIMARY KEY (id),
	CONSTRAINT estado_sigla_unique UNIQUE (sigla)
);

CREATE TABLE gestop.cidade (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	nome varchar(60) NOT NULL,
	estado_id int8 NOT NULL,
	CONSTRAINT cid_est_id_cid_uk UNIQUE (estado_id, nome),
	CONSTRAINT cidade_pkey PRIMARY KEY (id),
	CONSTRAINT estado_cidade_fk FOREIGN KEY (estado_id) REFERENCES gestop.estado(id)
);

CREATE TABLE gestop.endereco (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	bairro varchar(255) NOT NULL,
	cep varchar(8) NULL,
	complemento varchar(200) NULL,
	logradouro varchar(200) NOT NULL,
	numero varchar(6) NOT NULL,
	cidade_id int8 NOT NULL,
	CONSTRAINT endereco_pkey PRIMARY KEY (id),
	CONSTRAINT cidade_endereco_fk FOREIGN KEY (cidade_id) REFERENCES gestop.cidade(id)
);

CREATE TABLE gestop.empresa (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	cnpj varchar(18) NULL,
	email varchar(50) NOT NULL,
	telefone varchar(20) NULL,
	codigo varchar(10) NULL,
	inscricao_estadual varchar(20) NULL,
	inscricao_municipal varchar(20) NULL,
	nome_fantasia varchar(120) NOT NULL,
	razao_social varchar(120) NOT NULL,
	endereco_id int8 NOT NULL,
	CONSTRAINT emp_cnpj_uk UNIQUE (cnpj),
	CONSTRAINT emp_cod_uk UNIQUE (codigo),
	CONSTRAINT emp_insc_estadual_uk UNIQUE (inscricao_estadual),
	CONSTRAINT emp_insc_muni_uk UNIQUE (inscricao_municipal),
	CONSTRAINT empresa_pkey PRIMARY KEY (id),
	CONSTRAINT endereco_empresa_fk FOREIGN KEY (endereco_id) REFERENCES gestop.endereco(id)
);

CREATE TABLE gestop.fornecedor (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	cnpj varchar(18) NULL,
	telefone varchar(20) NULL,
	codigo varchar(10) NULL,
	inscricao_estadual varchar(20) NULL,
	inscricao_municipal varchar(20) NULL,
	nome_fantasia varchar(120) NOT NULL,
	razao_social varchar(120) NOT NULL,
	endereco_id int8 NULL,
	CONSTRAINT forn_cnpj_uk UNIQUE (cnpj),
	CONSTRAINT forn_cod_uk UNIQUE (codigo),
	CONSTRAINT forn_insc_estadual_uk UNIQUE (inscricao_estadual),
	CONSTRAINT forn_insc_muni_uk UNIQUE (inscricao_municipal),
	CONSTRAINT fornecedor_pkey PRIMARY KEY (id),
	CONSTRAINT endereco_fornecedor_fk FOREIGN KEY (endereco_id) REFERENCES gestop.endereco(id)
);

CREATE TABLE gestop.categoria (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	descricao varchar(50) NOT NULL,
	CONSTRAINT categoria_pkey PRIMARY KEY (id)
);

CREATE TABLE gestop.produto (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	referencia varchar(50) NOT NULL,
	nome varchar(80) NOT NULL,
	descricao text,
	observacao text,
    valor_compra DECIMAL(10, 2) NOT NULL,
    valor_venda DECIMAL(10, 2) NOT NULL,
    comissao numeric(10,2) NOT NULL,
	tipo_produto varchar(50) NOT NULL,
	categoria_id int8 NOT NULL,
	fornecedor_id int8,
	quantidade_estoque int4 NULL,
	foto varchar(100) NULL,
	content_type varchar(100) NULL,
	CONSTRAINT produto_pkey PRIMARY KEY (id),
	CONSTRAINT produto_fornecedor_fkey FOREIGN KEY (fornecedor_id) REFERENCES gestop.fornecedor(id),
	CONSTRAINT produto_categoria_fkey FOREIGN KEY (categoria_id) REFERENCES gestop.categoria(id)
);

CREATE TABLE gestop.cliente (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	nome varchar(80) NOT NULL,
	tipo_pessoa varchar(15) NOT NULL,
	cpf_cnpj varchar(30) NULL,
	telefone varchar(20) NULL,
	email varchar(50) NOT NULL,
	logradouro varchar(50) NULL,
	numero varchar(15) NULL,
	complemento varchar(20) NULL,
	cep varchar(15) NULL,
	endereco_id int8 NULL,
	CONSTRAINT cliente_pkey PRIMARY KEY (id),
	CONSTRAINT cliente_endereco_fkey FOREIGN KEY (endereco_id) REFERENCES gestop.cidade(id)
);

CREATE TABLE gestop.venda (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	valor_frete numeric(10,2) NULL,
	valor_desconto numeric(10,2) NULL,
	valor_total numeric(10,2) NOT NULL,
	status varchar(30) NOT NULL,
	observacao varchar(200) NULL,
	data_hora_entrega timestamp NULL,
	cliente_id int8 NOT NULL,
	usuario_id int8 NOT NULL,
	CONSTRAINT venda_pkey PRIMARY KEY (id),
	CONSTRAINT venda_cliente_fkey FOREIGN KEY (cliente_id) REFERENCES gestop.cliente(id),
	CONSTRAINT venda_usuario_fkey FOREIGN KEY (usuario_id) REFERENCES gestop.usuario(id)
);

CREATE TABLE gestop.item_venda (
	id bigserial NOT NULL,
	data_alteracao date NULL,
	data_cadastro date NULL,
	quantidade int4 NOT NULL,
	valor_unitario numeric(10,2) NOT NULL,
	produto_id int8 NOT NULL,
	venda_id int8 NOT NULL,
	CONSTRAINT item_venda_pkey PRIMARY KEY (id),
	CONSTRAINT item_venda_produto_fkey FOREIGN KEY (produto_id) REFERENCES gestop.produto(id),
	CONSTRAINT item_venda_venda_fkey FOREIGN KEY (venda_id) REFERENCES gestop.venda(id)
);


