package org.primefaces.verona.view;

import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named
@SessionScoped
public class GuestPreferences implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8945019474236601864L;

	private String layout = "flow";

    private String theme = "turquoise";

    private String menuMode = "static";

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getMenuMode() {
        return this.menuMode;
    }

    public void setMenuMode(String value) {
        this.menuMode = value;
    }
}
