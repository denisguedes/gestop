package br.com.optimized.generico.model.filter;

import java.io.Serializable;
import java.util.Date;

public class FiltroPesquisa<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public FiltroPesquisa(T entity) {
		super();
		this.entity = entity;
	}
	
	private String chaveBuscaContrato;
	private String valorBuscaContrato;
	private String chaveBuscaCampo;
	private String valorBuscaCampo;
	private String campoPesquisa;	
	private Long campoPesquisaId;
	private int primeiroRegistro;
	private int quantidadeRegistros;
	private String propriedadeOrdenacao;
	private boolean ascendente;
	private Date datatBusca;
	private String tipoFaturamento;
	private T entity;
	
	public void setFiltro(FiltroPesquisa<T> filtro) {
		this.campoPesquisa = filtro.campoPesquisa;
		this.campoPesquisaId = filtro.campoPesquisaId;
		this.primeiroRegistro = filtro.primeiroRegistro;
		this.quantidadeRegistros = filtro.quantidadeRegistros;
		this.propriedadeOrdenacao = filtro.propriedadeOrdenacao;
		this.ascendente = filtro.ascendente;
		this.chaveBuscaContrato = filtro.chaveBuscaContrato;
		this.valorBuscaContrato = filtro.valorBuscaContrato;
		this.datatBusca = filtro.datatBusca;
		this.tipoFaturamento = filtro.tipoFaturamento;
	}

	public FiltroPesquisa() {
		super();
	}

	public String getCampoPesquisa() {
		return campoPesquisa;
	}

	public void setCampoPesquisa(String campoPesquisa) {
		this.campoPesquisa = campoPesquisa;
	}

	public Long getCampoPesquisaId() {
		return campoPesquisaId;
	}

	public void setCampoPesquisaId(Long campoPesquisaId) {
		this.campoPesquisaId = campoPesquisaId;
	}

	public int getPrimeiroRegistro() {
		return primeiroRegistro;
	}

	public void setPrimeiroRegistro(int primeiroRegistro) {
		this.primeiroRegistro = primeiroRegistro;
	}

	public int getQuantidadeRegistros() {
		return quantidadeRegistros;
	}

	public void setQuantidadeRegistros(int quantidadeRegistros) {
		this.quantidadeRegistros = quantidadeRegistros;
	}

	public String getPropriedadeOrdenacao() {
		return propriedadeOrdenacao;
	}

	public void setPropriedadeOrdenacao(String propriedadeOrdenacao) {
		this.propriedadeOrdenacao = propriedadeOrdenacao;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}

	public String getChaveBuscaContrato() {
		return chaveBuscaContrato;
	}

	public void setChaveBuscaContrato(String chaveBuscaContrato) {
		this.chaveBuscaContrato = chaveBuscaContrato;
	}

	public String getValorBuscaContrato() {
		return valorBuscaContrato;
	}

	public void setValorBuscaContrato(String valorBuscaContrato) {
		this.valorBuscaContrato = valorBuscaContrato;
	}

	public Date getDatatBusca() {
		return datatBusca;
	}

	public void setDatatBusca(Date datatBusca) {
		this.datatBusca = datatBusca;
	}

	public String getChaveBuscaCampo() {
		return chaveBuscaCampo;
	}

	public void setChaveBuscaCampo(String chaveBuscaCampo) {
		this.chaveBuscaCampo = chaveBuscaCampo;
	}

	public String getValorBuscaCampo() {
		return valorBuscaCampo;
	}

	public void setValorBuscaCampo(String valorBuscaCampo) {
		this.valorBuscaCampo = valorBuscaCampo;
	}

	public String getTipoFaturamento() {
		return tipoFaturamento;
	}

	public void setTipoFaturamento(String tipoFaturamento) {
		this.tipoFaturamento = tipoFaturamento;
	}
}
