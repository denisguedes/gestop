package br.com.optimized.generico.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.Conversation;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.generico.model.AbstractModel;
import br.com.optimized.generico.service.IGenericBaseService;

public abstract class AbstractBean<S extends IGenericBaseService<E, T>, E extends AbstractModel<Long>, T> extends LazyDataModel<E> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1474689354710001522L;

	@Inject
	protected S service;
	@Inject 
	private Conversation conversation;
	protected LazyDataModel<E> resultList;
	protected List<E> selecionados;
	protected Logger logger = Logger.getLogger(this.getClass());
	protected FacesUtil facesUtils = new FacesUtil();
	protected E selecionado;
	protected String tipo;
	
	public AbstractBean() {
		super();
	}
	public LazyDataModel<E> getResultList() {
		return resultList;
	}

	public void setResultList(LazyDataModel<E> resultList) {
		this.resultList = resultList;
	}

	public void beginConversation() {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
		}
	}

	public void endConversation() {
		if (!this.conversation.isTransient()) {
			this.conversation.end();
		}
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
    @Override
    public E getRowData(String rowKey) {
    	Long id = Long.parseLong(rowKey);
        for(E entity : selecionados) {
            if(entity.equals(id))
                return entity;
        }
        return null;
    }
    
	public void onRowSelect() {
		
	}
    
	public List<E> getSelecionados() {
		return selecionados;
	}

	public void setSelecionados(List<E> selecionados) {
		this.selecionados = selecionados;
	}

	@Transactional
	public String salvar() {
		try {
			service.incluir(getSelecionado());
			selecionados = service.todos();
			facesUtils.msgFeedback(null, FacesUtil.msgBundle.getString("msgMergeSucesso"), FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",	FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return null;
		}
		return tipo + "List.xhtml?faces-redirect=true\"";
	}
	
	   public void onRowSelect(SelectEvent<E> event) {
		   setSelecionado(event.getObject());
	    }
	
	public String editar() {
		return "cadastro?" + tipo + "=" + getSelecionado().getId();
	}
	
	@Transactional
	public void excluir() {
		try {
			service.excluir(selecionado);
			selecionados = service.todos();
			FacesUtil.addInfoMessage("Registro " + selecionado.getId() + " excluído com sucesso.");
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",	FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();		}
	}
	
	public void selecionar(E model) {
	     setSelecionado(model);
	}

	public E getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(E selecionado) {
		this.selecionado = selecionado;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}