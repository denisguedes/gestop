package br.com.optimized.generico.service;

import java.util.List;

import br.com.optimized.generico.model.AbstractModel;

public interface IGenericBaseService<E extends AbstractModel<Long>, T> {

	E incluir(E entidade);
	E alterar(Long id, E entidade);
	void excluir(E entidade);
	E buscarPorId(T id);
	Object listar();
	List<E> todos();
		
}
