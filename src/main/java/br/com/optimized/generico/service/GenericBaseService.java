package br.com.optimized.generico.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.optimized.config.util.Transacional;
import br.com.optimized.generico.model.AbstractModel;

/*
* @param <R> Classe do repository da classe a ser manipulada.
* @param <E> Classe da entidade a ser manipulada.
* @param <T> Classe do tipo de dado do id da classe a ser manipulada.
*/

public abstract class GenericBaseService<R extends JpaRepository<E, T>, E extends AbstractModel<Long>, T extends Serializable>
		implements IGenericBaseService<E, T> {

	@Inject
	public R repository;

	// Novo
	@Transacional
	public E incluir(E entidade) {
		return repository.save(entidade);
	}

	// Editar
	@Transacional
	public E alterar(T id, E entidade) {
		E entidadeSalva = buscarPorId(id);
		BeanUtils.copyProperties(entidade, entidadeSalva, "id");
		return repository.save(entidadeSalva);
	}

	// remover
	@Transacional
	public void excluir(E entidade) {
		repository.delete(entidade);
	}

	// Buscar por código
	public E buscarPorId(T id) {
		Optional<E> entidade = repository.findById(id);
		if (entidade.isPresent()) {
			return entidade.get();
		}
		return null;
	}

	// Listar
	public Object listar() {
		return repository.findAll();
	}
	
	public List<E> todos(){
		return repository.findAll();
	}
}
