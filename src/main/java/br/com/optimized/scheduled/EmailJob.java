package br.com.optimized.scheduled;

import java.util.concurrent.atomic.AtomicLong;

import javax.inject.Singleton;

import de.mirkosertic.cdicron.api.Cron;

@Singleton
public class EmailJob {

	public static final AtomicLong COUNTER = new AtomicLong(0);

    @Cron(cronExpression = "0 04 00 ? * MON-FRI")
    public void scheduledMethod() {
        System.out.println("Processando...");
    }
}
