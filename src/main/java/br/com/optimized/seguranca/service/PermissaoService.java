package br.com.optimized.seguranca.service;

import java.io.Serializable;
import java.util.List;

import br.com.optimized.generico.service.GenericBaseService;
import br.com.optimized.seguranca.model.Permissao;
import br.com.optimized.seguranca.model.filter.PermissaoFilter;
import br.com.optimized.seguranca.repository.PermissaoRepository;

public class PermissaoService extends GenericBaseService<PermissaoRepository, Permissao, Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public List<Permissao> pesquisar(PermissaoFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(PermissaoFilter filtro) {
		return repository.total(filtro);
	}
	
	public List<Permissao> findAllByOrderByDescricao(){
		return repository.findAllByOrderByDescricao();
	}

}