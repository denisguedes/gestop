package br.com.optimized.seguranca.service;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import br.com.optimized.cadastro.model.Email;
import br.com.optimized.cadastro.model.dto.EmailDTO;
import br.com.optimized.cadastro.service.EmailService;
import br.com.optimized.config.exception.GlobalException;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.config.util.Transacional;
import br.com.optimized.generico.service.GenericBaseService;
import br.com.optimized.seguranca.controller.IdentityBean;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.model.filter.UsuarioFilter;
import br.com.optimized.seguranca.repository.UsuarioRepository;

@Service
public class UsuarioService extends GenericBaseService<UsuarioRepository, Usuario, Long> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private IdentityBean identityBean;
	@Inject
	private EmailService emailService;
	
	public List<Usuario> pesquisar(UsuarioFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(UsuarioFilter filtro) {
		return repository.total(filtro);
	}
	
    @Transacional
	public Usuario save(Usuario usuario, boolean novoUsuario, String senhaGerada, boolean geraNovaSenhaParaUsuario, Perfil perfil) throws GlobalException, Exception {
    	try {
		if(usuario.getPerfil() == null || usuario.getPerfil().getNome().isEmpty())
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackPerfilDeveSerInformado"), FacesMessage.SEVERITY_WARN);
		repository.save(usuario);
		if(novoUsuario || geraNovaSenhaParaUsuario)
			enviarEmailSenhaUsuario(usuario, senhaGerada);
    	}catch (PersistenceException e) {
    		mensagemPersistenceException(e);
    	}
		return usuario;
	}
    
	public List<Usuario> buscarUsuarioSolicitante(String login) {
		return repository.buscarUsuarioSolicitante(login, identityBean.getUsuarioLogado().getPerfil());
	}
	
	public void enviarEmailSenhaUsuario(Usuario usuario, String senhaGerada) 
			throws GlobalException, Exception {
		
		Email emailSmtp = emailService.retornarEmailSMTP();
		
		EmailDTO emailToTO = new EmailDTO();
		emailToTO.setSubject(FacesUtil.msgBundle.getString("msgTituloEmailEnviadoSucessoCadUsuario"));
		emailToTO.setText(FacesUtil.getString("msgTextoEmailEnviadoSucessoCadUsuario", null, usuario.getLogin(), senhaGerada));
		emailToTO.setEmailTo(usuario.getEmail());
		
		emailService.enviaEmail(emailSmtp, emailToTO);
	}

	public List<String> mensagemPersistenceException(PersistenceException e) {
		List<String> listaMsgFeedBack = new ArrayList<>();
		if (!isConstraintViolationException(e)) {
			listaMsgFeedBack.add(FacesUtil.msgBundle.getString("msgGenericaAlert"));
			
		} else {
			ConstraintViolationException causa = (ConstraintViolationException) e.getCause();

			String validaLoginUnico = validarLoginUnicoUsuario(causa);
			String validaEmailUnico = validarEmailUnicoUsuario(causa);
			String validaUsuarioComAprovacaoPendente = validarUsuarioComAprovacaoPendente(causa);
			
			if(validaLoginUnico != null) {
				listaMsgFeedBack.add(validaLoginUnico);
			}
			if(validaEmailUnico != null) {
				listaMsgFeedBack.add(validaEmailUnico);
			}
			if(validaUsuarioComAprovacaoPendente != null) {
				listaMsgFeedBack.add(validaUsuarioComAprovacaoPendente);
			}
		}
		listaMsgFeedBack.remove(null);
		return listaMsgFeedBack;
	}
	
	public boolean isConstraintViolationException(PersistenceException e) {
		if (e.getCause() instanceof ConstraintViolationException) {
			return true;
		} else {
			return false;
		}
	}
	
	public String validarLoginUnicoUsuario(ConstraintViolationException causa) {
		if (causa.getConstraintName().equalsIgnoreCase("siconjur.usuario_login_unique")) {
			return FacesUtil.msgBundle.getString("msgGenericaAlert") + " "
					+ FacesUtil.msgBundle.getString("msgFeedbackUsuarioLoginUnique");
		} else {
			return null;
		}
	}
	
	public String validarEmailUnicoUsuario(ConstraintViolationException causa) {
		if (causa.getConstraintName().equalsIgnoreCase("siconjur.usuario_email_unique")) {
			return FacesUtil.msgBundle.getString("msgGenericaAlert") + " "
					+ FacesUtil.msgBundle.getString("msgFeedbackUsuarioEmailUnique");
		} else {
			return null;
		}
	}
	
	public String validarUsuarioComAprovacaoPendente(ConstraintViolationException causa) {
		if (causa.getConstraintName().equalsIgnoreCase("siconjur.gp_aprov_usu_emp_ue_fk")) {
			return FacesUtil.msgBundle.getString("msgFeedbackExcluirEmpresaAssociadaAoUsuarioComAprovacaoPendente");
		} else {
			return null;
		}
	}
	
	public String verificaSenhaAtual(String senhaAtual, String novaSenha, String repeteNovaSenha) 
			throws GlobalException {
		if(FacesUtil.compararSenha(senhaAtual, identityBean.getUsuarioLogado().getSenha())) {
			try {
				return verificaSenhaForteEmNovaSenhaERepeteSenha(novaSenha, repeteNovaSenha);
			} catch (UnsupportedEncodingException e) {
				throw new GlobalException(e.getMessage(), FacesMessage.SEVERITY_ERROR);
			} catch (NoSuchAlgorithmException e) {
				throw new GlobalException(e.getMessage(), FacesMessage.SEVERITY_ERROR);
			}
		} else {
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioSenhaAtualIncorreta"), FacesMessage.SEVERITY_WARN);
		}
	}
	
	public String verificaSenhaForteEmNovaSenhaERepeteSenha(String novaSenha, String repeteNovaSenha) 
			throws UnsupportedEncodingException, NoSuchAlgorithmException, GlobalException {
		if(FacesUtil.senhaForte(novaSenha) && FacesUtil.senhaForte(repeteNovaSenha)) {
				return verificaIgualdadeNovaSenhaERepeteSenha(novaSenha, repeteNovaSenha);
		} else {
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioSenhaForte"), FacesMessage.SEVERITY_WARN);
		}
	}

	private String verificaIgualdadeNovaSenhaERepeteSenha(String novaSenha, String repeteNovaSenha) 
			throws UnsupportedEncodingException, NoSuchAlgorithmException, GlobalException {
		if(novaSenha.equals(repeteNovaSenha)) {
			this.identityBean.getUsuarioLogado().setSenha(FacesUtil.criptografarSenha(novaSenha));
			repository.save(this.identityBean.getUsuarioLogado());
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioSenhaAtualizadaComSucesso"), FacesMessage.SEVERITY_INFO);
		} else {
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioNovaSenhaERepeteNovaSenhaDiferente"), FacesMessage.SEVERITY_WARN);
		}
	}
	
	public Usuario findUsuario(String login) throws GlobalException {
		Usuario usuario = repository.findByLogin(login);
		if(usuario == null)
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioInexistente"), FacesMessage.SEVERITY_ERROR);
		return usuario;
	}
	
	public Usuario atualizaNumeroTentativasErradasUsuario(Usuario usuario) {
		usuario.setNumeroTentativasLogin(usuario.getNumeroTentativasLogin()+1);
		usuario.setUltimaTentativaLogin(new Date());
		
		return repository.save(usuario);
	}

}
