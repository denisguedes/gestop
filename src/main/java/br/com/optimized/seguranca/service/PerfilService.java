package br.com.optimized.seguranca.service;

import java.io.Serializable;
import java.util.List;

import br.com.optimized.generico.service.GenericBaseService;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.filter.PerfilFilter;
import br.com.optimized.seguranca.repository.PerfilRepository;

public class PerfilService extends GenericBaseService<PerfilRepository, Perfil, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Perfil> pesquisar(PerfilFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(PerfilFilter filtro) {
		return repository.total(filtro);
	}

}