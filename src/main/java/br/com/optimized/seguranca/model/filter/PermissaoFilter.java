package br.com.optimized.seguranca.model.filter;

import java.io.Serializable;

import br.com.optimized.generico.model.filter.FiltroPesquisa;

public class PermissaoFilter extends FiltroPesquisa<Object> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1447753381971440642L;
	
	private Long id;
	private String descricao;
	
	public PermissaoFilter() {
	}
	
	public PermissaoFilter(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
