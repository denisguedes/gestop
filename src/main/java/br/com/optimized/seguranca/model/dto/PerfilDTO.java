package br.com.optimized.seguranca.model.dto;

public class PerfilDTO {

	protected Long id;
	private String nome;
	private Long idPerfil;
	
	//auditoria
	private Long numRevisao;
	private String dataHoraAlteracao;
	private String cpfUsuarioResponsavel;

	public PerfilDTO(Long id, String nome, Long idPerfil) {
		super();
		this.id = id;
		this.nome = nome;
		this.idPerfil = idPerfil;
	}

	public PerfilDTO(String nome, Long idPerfil) {
		this.nome = nome;
		this.idPerfil = idPerfil;
	}
	
	public PerfilDTO(String nome, Long idPerfil, Long numRevisao, String dataHoraAlteracao,
			String cpfUsuarioResponsavel) {
		this.nome = nome;
		this.idPerfil = idPerfil;
		this.numRevisao = numRevisao;
		this.dataHoraAlteracao = dataHoraAlteracao;
		this.cpfUsuarioResponsavel = cpfUsuarioResponsavel;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Long idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getDataHoraAlteracao() {
		return dataHoraAlteracao;
	}

	public void setDataHoraAlteracao(String dataHoraAlteracao) {
		this.dataHoraAlteracao = dataHoraAlteracao;
	}

	public String getCpfUsuarioResponsavel() {
		return cpfUsuarioResponsavel;
	}

	public void setCpfUsuarioResponsavel(String cpfUsuarioResponsavel) {
		this.cpfUsuarioResponsavel = cpfUsuarioResponsavel;
	}

	public Long getNumRevisao() {
		return numRevisao;
	}

	public void setNumRevisao(Long numRevisao) {
		this.numRevisao = numRevisao;
	}
	
	
}

