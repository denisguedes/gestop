package br.com.optimized.seguranca.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name="perfil", schema = "gestop")
public class Perfil extends AbstractModel<Long> implements Serializable {
	
	private static final long serialVersionUID = 6040744910073816072L;
	
	@NotEmpty
	@Column(name="nome")
	private String nome;
	
	@JsonManagedReference("permissoes")
	@OneToMany(mappedBy = "perfil", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)	
	private List<PerfilPermissao> permissoes = new ArrayList<>();
    
	public Perfil(Long id, String nome) {
		super.id = id;
		this.nome = nome;
	}
	
	public Perfil() {
		super();
		this.permissoes = new ArrayList<>();
	}

	public Perfil(String nome) {
		super();
		this.nome = nome;
	}
	
	public Perfil(String nome, List<PerfilPermissao> permissoes) {
		super();
		this.nome = nome;
		this.permissoes = permissoes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<PerfilPermissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<PerfilPermissao> permissoes) {
		this.permissoes = permissoes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}