package br.com.optimized.seguranca.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "usuario", schema = "gestop", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "login" }, name = "usuario_login_unique"),
		@UniqueConstraint(columnNames = { "email" }, name = "usuario_email_unique") })
public class Usuario extends AbstractModel<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6222893889676191863L;

	@NotEmpty
	@Column(name = "login", length = 10)
	private String login;

	@NotEmpty
	@Column(name = "senha", length = 255)	private String senha;

	@CPF(message="Informe um CPF válido!")
	@Column(name = "cpf", length = 14)
	private String cpf;

	@NotEmpty
	@Column(name = "nome", length = 60)
	private String nome;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "data_nascimento")
	private Date dataNascimento;

	@NotEmpty
	@Column(name = "identificador", length = 7)
	private String identificador;

	@Email(message = Constantes.MSG_FEEDBACK_USUARIO_EMAIL_INVALIDO)
	@NotEmpty
	@Column(name = "email", length = 60)
	private String email;

	@NotEmpty
	@Column(name = "setor", length = 60)
	private String setor;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "perfil_id", referencedColumnName = "id")
	private Perfil perfil;

	@Column(name = "tentativa_login", length = 5, columnDefinition = "int default '0'")
	private int numeroTentativasLogin;

	@NotNull
	@Column(name = "ultima_tentativa_login")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date ultimaTentativaLogin;

	@NotNull
	@Column(name = "super_administrador")
	private Boolean superAdministrador = false;

	public Usuario() {
		super();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login.toLowerCase();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getNumeroTentativasLogin() {
		return numeroTentativasLogin;
	}

	public void setNumeroTentativasLogin(int numeroTentativasLogin) {
		this.numeroTentativasLogin = numeroTentativasLogin;
	}

	public Date getUltimaTentativaLogin() {
		return ultimaTentativaLogin;
	}

	public void setUltimaTentativaLogin(Date ultimaTentativaLogin) {
		this.ultimaTentativaLogin = ultimaTentativaLogin;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Boolean getSuperAdministrador() {
		return superAdministrador;
	}

	public void setSuperAdministrador(Boolean superAdministrador) {
		this.superAdministrador = superAdministrador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}

}