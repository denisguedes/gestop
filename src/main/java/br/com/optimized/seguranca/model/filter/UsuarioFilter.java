package br.com.optimized.seguranca.model.filter;

import java.io.Serializable;

import br.com.optimized.generico.model.filter.FiltroPesquisa;

/**
 * @author Daniel Correia
 * @since 28.03.2016
 *
 */
public class UsuarioFilter extends FiltroPesquisa<Object> implements Serializable {

	private static final long serialVersionUID = 1L;

	private String campoPesquisaCpf;
	private String campoPesquisaLogin;
	private String campoPesquisaNome;

	public String getCampoPesquisaCpf() {
		return campoPesquisaCpf;
	}

	public void setCampoPesquisaCpf(String campoPesquisaCpf) {
		this.campoPesquisaCpf = campoPesquisaCpf;
	}

	public String getCampoPesquisaLogin() {
		return campoPesquisaLogin;
	}

	public void setCampoPesquisaLogin(String campoPesquisaLogin) {
		this.campoPesquisaLogin = campoPesquisaLogin;
	}

	public String getCampoPesquisaNome() {
		return campoPesquisaNome;
	}

	public void setCampoPesquisaNome(String campoPesquisaNome) {
		this.campoPesquisaNome = campoPesquisaNome;
	}
	
}
