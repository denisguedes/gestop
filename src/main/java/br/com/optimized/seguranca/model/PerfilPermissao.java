package br.com.optimized.seguranca.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "perfil_permissao", schema = "gestop")
public class PerfilPermissao extends AbstractModel<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -729474655353246399L;

	@ManyToOne
	@JoinColumn(name = "perfil_id")
	@JsonBackReference("permissoes")
	private Perfil perfil;

	@ManyToOne
	@JoinColumn(name = "permissao_id")
	private Permissao permissao;

	
	public PerfilPermissao() {
		super();
	}

	public PerfilPermissao(Perfil perfil, Permissao permissao) {
		super();
		this.perfil = perfil;
		this.permissao = permissao;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilPermissao other = (PerfilPermissao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
