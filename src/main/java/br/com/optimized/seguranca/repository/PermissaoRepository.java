package br.com.optimized.seguranca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.seguranca.model.Permissao;
import br.com.optimized.seguranca.repository.query.PermissaoRepositoryQuery;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long>, PermissaoRepositoryQuery {

	List<Permissao> findAllByOrderByDescricao();

}
