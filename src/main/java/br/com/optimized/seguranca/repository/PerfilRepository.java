package br.com.optimized.seguranca.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.repository.query.PerfilRepositoryQuery;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long>, PerfilRepositoryQuery {

	List<Perfil> findAllByOrderByNome();
	
}
