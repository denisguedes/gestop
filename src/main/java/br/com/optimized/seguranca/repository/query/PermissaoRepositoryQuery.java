package br.com.optimized.seguranca.repository.query;

import java.util.List;

import br.com.optimized.seguranca.model.Permissao;
import br.com.optimized.seguranca.model.filter.PermissaoFilter;

public interface PermissaoRepositoryQuery {

	public List<Permissao> pesquisar(PermissaoFilter filtro);
	
	public Long total(PermissaoFilter filtro);	
}
