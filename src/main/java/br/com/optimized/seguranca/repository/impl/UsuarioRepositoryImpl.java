package br.com.optimized.seguranca.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.model.filter.UsuarioFilter;
import br.com.optimized.seguranca.repository.query.UsuarioRepositoryQuery;

public class UsuarioRepositoryImpl implements UsuarioRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	protected Logger logger = Logger.getLogger(this.getClass());
	
	@Override
	public List<Usuario> pesquisar(UsuarioFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = builder.createQuery(Usuario.class);
		Root<Usuario> root = criteria.from(Usuario.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Usuario> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(UsuarioFilter filtro, CriteriaBuilder builder,
			Root<Usuario> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getCampoPesquisaNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getCampoPesquisaNome().toLowerCase() + "%"));
		}	
		if (!StringUtils.isEmpty(filtro.getCampoPesquisaLogin())) {
			predicates.add(builder.like(builder.lower(root.get("login")),
					"%" + filtro.getCampoPesquisaLogin().toLowerCase() + "%"));
		}
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, UsuarioFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(UsuarioFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Usuario> root = criteria.from(Usuario.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	public List<Usuario> buscarUsuarioSolicitante(String login, Perfil perfil) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select distinct u.* ");
		jpql.append(" from usuario u ");
		jpql.append(" where u.identificador = :ativo ");
		jpql.append(" and u.perfil.id = :idPerfil ");
	    TypedQuery<Usuario> query = manager.createQuery(jpql.toString(), Usuario.class);
		query.setParameter("ativo", Constantes.USUARIO_ATIVO);
		query.setParameter("idPerfil", perfil.getId());
		List<Usuario> list = query.getResultList();
		return list;
	}

	@Override
	public List<Usuario> envioEmailUsuario(String cpf) {
		// TODO Auto-generated method stub
		return null;
	}

}
