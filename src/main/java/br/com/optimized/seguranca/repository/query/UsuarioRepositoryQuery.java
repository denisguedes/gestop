package br.com.optimized.seguranca.repository.query;

import java.util.List;

import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.model.filter.UsuarioFilter;

public interface UsuarioRepositoryQuery {

	public List<Usuario> pesquisar(UsuarioFilter filtro);
	
	public Long total(UsuarioFilter filtro);
	
	public List<Usuario> envioEmailUsuario(String cpf);
	
	public List<Usuario> buscarUsuarioSolicitante(String login, Perfil perfil);
	
}
