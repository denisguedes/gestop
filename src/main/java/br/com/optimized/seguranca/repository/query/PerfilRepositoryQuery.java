package br.com.optimized.seguranca.repository.query;

import java.util.List;

import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.filter.PerfilFilter;

public interface PerfilRepositoryQuery {

	public List<Perfil> pesquisar(PerfilFilter filtro);
	
	public Long total(PerfilFilter filtro);	
}
