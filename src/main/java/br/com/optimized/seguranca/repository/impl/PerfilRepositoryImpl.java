package br.com.optimized.seguranca.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;

import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.dto.PerfilDTO;
import br.com.optimized.seguranca.model.filter.PerfilFilter;
import br.com.optimized.seguranca.repository.query.PerfilRepositoryQuery;

public class PerfilRepositoryImpl implements PerfilRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<Perfil> pesquisar(PerfilFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Perfil> criteria = builder.createQuery(Perfil.class);
		Root<Perfil> root = criteria.from(Perfil.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Perfil> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(PerfilFilter filtro, CriteriaBuilder builder,
			Root<Perfil> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}	
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, PerfilFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(PerfilFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Perfil> root = criteria.from(Perfil.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	

	public List<PerfilDTO> findPerfilsAuditadosByPerfil(Perfil selecionado) {
		AuditReader auditReader = AuditReaderFactory.get(manager);
		AuditQuery q = auditReader.createQuery().forRevisionsOfEntity(Perfil.class, false, true);
		q.add(AuditEntity.id().eq(selecionado.getId()));
		q.addOrder(AuditEntity.id().desc());
		@SuppressWarnings("unchecked")
		List<Object[]> audit = q.getResultList();
		
		List<PerfilDTO> listPerfilDTO = new ArrayList<>();
		for(Object [] obj : audit) {
			Perfil perfil = (Perfil) obj[0];
			br.com.optimized.auditoria.AuditEntity auditoria = (br.com.optimized.auditoria.AuditEntity) obj[1];
			PerfilDTO perfilTO = new PerfilDTO(perfil.getNome(), null, auditoria.getId(), 
					FacesUtil.converteTimestampToDataHora(auditoria.getDataTimestamp()), auditoria.getCpfUsuario());
			listPerfilDTO.add(perfilTO);
		}
		return listPerfilDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<Perfil> findByEmpresaLogada(Long empresaLogadaId) {
		try {
			StringBuffer jpql = new StringBuffer();
			jpql.append(" select r.id, r.nome ");
			jpql.append("   from siconjur.perfil r ");
			jpql.append("  inner join empresa_perfil er ");
			jpql.append("     on r.id = er.perfils_id ");
			jpql.append("  where er.empresa_id = :empresaLogadaId ");
			Query query = manager.createNativeQuery(jpql.toString());
			query.setParameter("empresaLogadaId", empresaLogadaId);
			List<Object[]> list = query.getResultList();
			List<Perfil> listResult = new ArrayList<Perfil>();
			list.stream().forEach(item -> listResult.add(new Perfil(FacesUtil.validaLong(item[0]), FacesUtil.validaString(item[1]))));
			return listResult;
		} catch (NoResultException e) {
			return null;
		}
	}

}
