package br.com.optimized.seguranca.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.repository.query.UsuarioRepositoryQuery;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryQuery {

	public Usuario findFirstByEmail(String email);

	public Optional<Usuario>findByEmail(String email);

	public Usuario findFirstByNome(String nome);	
	
	public Usuario findByCpf(String cpf);	
	
	public Usuario findByLogin(String cpf);	

}