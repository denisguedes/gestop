package br.com.optimized.seguranca.controller;

import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;

import br.com.optimized.config.exception.GlobalException;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.generico.controller.AbstractBean;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.service.UsuarioService;

@Named
@ConversationScoped
public class EditarSenhaUsuarioBean extends AbstractBean<UsuarioService, Usuario, Long>{

	private static final long serialVersionUID = 8624419038600490656L;
	
	@Inject private UsuarioService usuarioService;
	
	private Logger logger = Logger.getLogger(this.getClass());
	private FacesUtil facesUtils = new FacesUtil();
	
	private String senhaAtual;
	private String novaSenha;
	private String repeteNovaSenha;
	private String urlFormEditaSenha = "/usuario/editar-senha.xhtml?faces-redirect=true";
	
	public String editarSenha() {
		try {
			this.usuarioService.verificaSenhaAtual(this.senhaAtual, this.novaSenha, this.repeteNovaSenha);
		} catch (GlobalException e) {
			this.facesUtils.msgFeedback(null, e.getMsg(), e.getTipoMensagem());
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		} 
		return urlFormEditaSenha;
	}
	
	public void limparFormEditarSenhaUsuario() {
		this.senhaAtual = null;
		this.novaSenha = null;
		this.repeteNovaSenha = null;
	}
	
	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getRepeteNovaSenha() {
		return repeteNovaSenha;
	}

	public void setRepeteNovaSenha(String repeteNovaSenha) {
		this.repeteNovaSenha = repeteNovaSenha;
	}
}
