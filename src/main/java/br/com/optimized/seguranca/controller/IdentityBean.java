package br.com.optimized.seguranca.controller;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.core.context.SecurityContextHolder;

import br.com.optimized.config.security.UsuarioSistema;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.PerfilPermissao;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.repository.PerfilRepository;

@Named
@ViewScoped
public class IdentityBean implements Serializable {

	private static final long serialVersionUID = -6094950580241799389L;
	
	@Inject
	private PerfilRepository perfilRepository;

	public IdentityBean() {
	}

	public Usuario getUsuarioLogado() {
		UsuarioSistema usuarioSistema = (UsuarioSistema) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		return usuarioSistema.getUsuario();
	}

	public boolean hasPermission(String permission) {
		if(getUsuarioLogado().getSuperAdministrador()) {
			return true;
		}
		boolean hasPermission = false;
		Usuario usuario = getUsuarioLogado();
		
		Perfil perfil = perfilRepository.findById(usuario.getPerfil().getId()).orElse(new Perfil());
		
		for (PerfilPermissao permissao : perfil.getPermissoes()) {
			if (permissao.getPermissao().getDescricao().equals(permission)) {
				hasPermission = true;
			}
		}
		return hasPermission;
	}

}