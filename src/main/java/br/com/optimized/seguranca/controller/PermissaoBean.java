package br.com.optimized.seguranca.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.optimized.generico.controller.AbstractBean;
import br.com.optimized.seguranca.model.Permissao;
import br.com.optimized.seguranca.model.filter.PermissaoFilter;
import br.com.optimized.seguranca.service.PermissaoService;

@Named
@ConversationScoped
public class PermissaoBean  extends AbstractBean<PermissaoService, Permissao, Long>{

	private static final long serialVersionUID = 1L;
	
	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "permissaoForm.xhtml" + PARAM_REDIRECT;
	private static final String URL_LIST = "permissaoList.xhtml" + PARAM_REDIRECT;

	private PermissaoFilter filtro;
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private LazyDataModel<Permissao> model;
	
	public PermissaoBean() {
		filtro = new PermissaoFilter();
		model = new LazyDataModel<Permissao>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Permissao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		setTipo("permissao");
		inicializaLista();
	}
	
	public void inicializaLista() {
		try {
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	public String novo() {
		setSelecionado(new Permissao());
		return URL_FORM;
	}
	
	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public String editar() {
		this.renderizaVisualizar = false;
		setSelecionado(service.buscarPorId(getSelecionado().getId()));
		return URL_FORM;
	}
	
	public String formVisualizar() {
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public PermissaoFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(PermissaoFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Permissao> getModel() {
		return model;
	}

	public Boolean getRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(Boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

}
