package br.com.optimized.seguranca.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.generico.controller.AbstractBean;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.PerfilPermissao;
import br.com.optimized.seguranca.model.Permissao;
import br.com.optimized.seguranca.model.filter.PerfilFilter;
import br.com.optimized.seguranca.service.PerfilService;
import br.com.optimized.seguranca.service.PermissaoService;

@Named
@ConversationScoped
public class PerfilBean extends AbstractBean<PerfilService, Perfil, Long>{

	private static final long serialVersionUID = 1L;

	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "perfilForm.xhtml" + PARAM_REDIRECT;
	private static final String URL_LIST = "perfilList.xhtml" + PARAM_REDIRECT;

	@Inject
	private PermissaoService permissaoService;
	private PerfilFilter filtro = new PerfilFilter();
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private DualListModel<Permissao> permissoes;
	private List<Permissao> listaPermissao;
	private List<Permissao> permissoesSelecionadas;
	private LazyDataModel<Perfil> model;
	
	public PerfilBean() {
		filtro = new PerfilFilter();
		model = new LazyDataModel<Perfil>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Perfil> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		setTipo("perfil");
		inicializaLista();
	}
	
	public void inicializaLista() {
		try {
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	public String novo() {
		inicializar();
		setSelecionado(new Perfil());
		return URL_FORM;
	}
	
	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public String editar() {
		inicializar();
		this.renderizaVisualizar = false;
		setSelecionado(service.buscarPorId(getSelecionado().getId()));
		return URL_FORM;
	}
	
	public String formVisualizar() {
		inicializar();
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
	@Transactional
	public String salvar() {
		return URL_LIST;
	}

	public void inicializar() {
		listaPermissao = permissaoService.todos();
		permissoesSelecionadas = new ArrayList<Permissao>();
		
		if (getSelecionado() == null) {
			limpar();
		}else {
			for(PerfilPermissao perfilPermissao : getSelecionado().getPermissoes()) {
				permissoesSelecionadas.add(perfilPermissao.getPermissao());
				listaPermissao.remove(perfilPermissao.getPermissao());
			}
		}
		permissoes = new DualListModel<Permissao>(listaPermissao, permissoesSelecionadas);
	}
	
	private void limpar() {
		selecionado = new Perfil();
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public PerfilFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(PerfilFilter filtro) {
		this.filtro = filtro;
	}

	public DualListModel<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(DualListModel<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public LazyDataModel<Perfil> getModel() {
		return model;
	}

	public List<Permissao> getListaPermissao() {
		return listaPermissao;
	}

	public void setListaPermissao(List<Permissao> listaPermissao) {
		this.listaPermissao = listaPermissao;
	}

	public List<Permissao> getPermissoesSelecionadas() {
		return permissoesSelecionadas;
	}

	public void setPermissoesSelecionadas(List<Permissao> permissoesSelecionadas) {
		this.permissoesSelecionadas = permissoesSelecionadas;
	}

	public Boolean getRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(Boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

}
