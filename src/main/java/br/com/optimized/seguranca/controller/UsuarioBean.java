package br.com.optimized.seguranca.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.RandomStringUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.optimized.config.exception.GlobalException;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.generico.controller.AbstractBean;
import br.com.optimized.seguranca.model.Perfil;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.model.filter.UsuarioFilter;
import br.com.optimized.seguranca.service.PerfilService;
import br.com.optimized.seguranca.service.UsuarioService;

@Named
@ConversationScoped
public class UsuarioBean extends AbstractBean<UsuarioService, Usuario, Long>{

	private static final long serialVersionUID = -6094950580241799389L;
	
	@Inject 
	private IdentityBean identityBean;
	@Inject
	private PerfilService perfilService;
	private List<Perfil> perfis = new ArrayList<>();
	private Perfil perfil = new Perfil();
	
	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "usuarioForm.xhtml"+PARAM_REDIRECT;
	private static final String URL_LIST = "usuarioList.xhtml"+PARAM_REDIRECT;
	
	private UsuarioFilter filtro = new UsuarioFilter();
	private String repeteNovaSenha;
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private Boolean renderizaCampoSenhaSeSuperAdmin = Boolean.FALSE;
	private Boolean renderizaCampoEnviarNovaSenha = Boolean.FALSE;
	private Boolean geraNovaSenhaParaUsuario = Boolean.FALSE;
	private Boolean cadastrarNovaSenha = Boolean.FALSE;
	private Boolean renderizaCadastrarNovaSenha = Boolean.FALSE;
	private Boolean btnAdicionarFiliaisNaLista = Boolean.TRUE;
	private LazyDataModel<Usuario> model;
	
	public UsuarioBean() {
		filtro = new UsuarioFilter();
		model = new LazyDataModel<Usuario>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Usuario> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {

				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		inicializaLista();
	}
	
	public void inicializaLista() {
		try {
			perfis = perfilService.todos();
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}

	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public String novo() {
		setSelecionado(new Usuario());
		renderizaCampoEnviarNovaSenha = false;
		return URL_FORM;
	}
	
	public String editar() {
		renderizaCampoEnviarNovaSenha = true;
		if(identityBean.getUsuarioLogado().getSuperAdministrador()) {
			cadastrarNovaSenha = false;
			renderizaCadastrarNovaSenha = true;
			selecionado.setSenha(null);
			renderizaCampoSenhaSeSuperAdmin = false;
			renderizaCampoEnviarNovaSenha = false;
		}
		setSelecionado(service.buscarPorId(getSelecionado().getId()));
		return URL_FORM;
	}
	
	public String formVisualizar() {
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
	public String salvar() {
		try {
			final boolean NOVO_USUARIO = getSelecionado().getId() == null;
			String gerarSenha = null;
			if(NOVO_USUARIO) {
				gerarSenha = generatePasswd();
				getSelecionado().setSenha(FacesUtil.criptografarSenha(gerarSenha));
				getSelecionado().setUltimaTentativaLogin(new Date());
			}else if(identityBean.getUsuarioLogado().getSuperAdministrador()) {
				if(cadastrarNovaSenha) {
					if(FacesUtil.senhaForte(getSelecionado().getSenha()) && FacesUtil.senhaForte(repeteNovaSenha)) {
						if(getSelecionado().getSenha().equals(repeteNovaSenha)) {
							getSelecionado().setSenha(FacesUtil.criptografarSenha(repeteNovaSenha));
						}else {
							throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioNovaSenhaERepeteNovaSenhaDiferente"), FacesMessage.SEVERITY_WARN);
						}
					}else {
						throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioSenhaForte"), FacesMessage.SEVERITY_WARN);
					}
				}
			}
			
			if(geraNovaSenhaParaUsuario) {
				gerarSenha = generatePasswd();
				getSelecionado().setSenha(FacesUtil.criptografarSenha(gerarSenha));
			}
			
			setSelecionado(this.service.save(getSelecionado(), NOVO_USUARIO, gerarSenha, geraNovaSenhaParaUsuario, perfil));
			facesUtils.msgFeedback(null, FacesUtil.msgBundle.getString("msgMergeSucesso"), FacesMessage.SEVERITY_INFO);
			
			if(NOVO_USUARIO || geraNovaSenhaParaUsuario) {
				facesUtils.msgFeedback(null, FacesUtil.msgBundle.getString("msgFeedbackUsuarioEmailEnviadoSucessoCadUsuario"), FacesMessage.SEVERITY_INFO);
			}
			
			endConversation();
			return URL_LIST;
		} catch (GlobalException e) {
			e.printStackTrace();
			facesUtils.msgFeedback(null , e.getMsgs(), FacesMessage.SEVERITY_WARN);
			return URL_FORM;
		}	catch (PersistenceException ep) {
			ep.printStackTrace();
			for (String msg : service.mensagemPersistenceException(ep)) {
				if(msg != null) {
					facesUtils.msgFeedback(null , msg , FacesMessage.SEVERITY_WARN);
				}
			}
			return URL_FORM;
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return URL_FORM;
		}
	}
	
	public void gerarNovaSenha() {
		getSelecionado().setSenha(FacesUtil.criptografarSenha(generatePasswd()));
	}
	
	public void habilitaCampoSenhaERepeteNovaSenha() {
		if(cadastrarNovaSenha) 
			renderizaCampoSenhaSeSuperAdmin = true;
		else 
			renderizaCampoSenhaSeSuperAdmin = false;
	}
	
	public void cancelar() {
		try {
			PrimeFaces.current().resetInputs(":formUsuario");
			clean();
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	public void clean() {
		this.perfil = null;
		setSelecionado(new Usuario());
	}
	
	public String generatePasswd() {
	    int length = 10;
	    boolean useLetters = true;
	    boolean useNumbers = false;
	    String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
	    return generatedString;
	}
	
	public List<Usuario> autoCompleteUsuarioSolicitante(String login){
		try {
			return this.service.buscarUsuarioSolicitante(login);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return new ArrayList<Usuario>();
		}
	}
	
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public UsuarioFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(UsuarioFilter filtro) {
		this.filtro = filtro;
	}

	public boolean isRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

	public boolean isRenderizaCampoSenhaSeSuperAdmin() {
		return renderizaCampoSenhaSeSuperAdmin;
	}

	public void setRenderizaCampoSenhaSeSuperAdmin(boolean renderizaCampoSenhaSeSuperAdmin) {
		this.renderizaCampoSenhaSeSuperAdmin = renderizaCampoSenhaSeSuperAdmin;
	}

	public String getRepeteNovaSenha() {
		return repeteNovaSenha;
	}

	public void setRepeteNovaSenha(String repeteNovaSenha) {
		this.repeteNovaSenha = repeteNovaSenha;
	}

	public Boolean getGeraNovaSenhaParaUsuario() {
		return geraNovaSenhaParaUsuario;
	}

	public void setGeraNovaSenhaParaUsuario(Boolean geraNovaSenhaParaUsuario) {
		this.geraNovaSenhaParaUsuario = geraNovaSenhaParaUsuario;
	}

	public boolean isRenderizaCampoEnviarNovaSenha() {
		return renderizaCampoEnviarNovaSenha;
	}

	public void setRenderizaCampoEnviarNovaSenha(boolean renderizaCampoEnviarNovaSenha) {
		this.renderizaCampoEnviarNovaSenha = renderizaCampoEnviarNovaSenha;
	}

	public Boolean getCadastrarNovaSenha() {
		return cadastrarNovaSenha;
	}

	public void setCadastrarNovaSenha(Boolean cadastrarNovaSenha) {
		this.cadastrarNovaSenha = cadastrarNovaSenha;
	}

	public Boolean getRenderizaCadastrarNovaSenha() {
		return renderizaCadastrarNovaSenha;
	}

	public void setRenderizaCadastrarNovaSenha(Boolean renderizaCadastrarNovaSenha) {
		this.renderizaCadastrarNovaSenha = renderizaCadastrarNovaSenha;
	}

	public Boolean getBtnAdicionarFiliaisNaLista() {
		return btnAdicionarFiliaisNaLista;
	}

	public void setBtnAdicionarFiliaisNaLista(Boolean btnAdicionarFiliaisNaLista) {
		this.btnAdicionarFiliaisNaLista = btnAdicionarFiliaisNaLista;
	}

	public LazyDataModel<Usuario> getModel() {
		return model;
	}

}
