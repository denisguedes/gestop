package br.com.optimized.seguranca.controller;


import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import br.com.optimized.config.security.AlertaUtil;

@Named
@SessionScoped
public class LoginBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private AlertaUtil msg = new AlertaUtil();
	protected Logger logger = Logger.getLogger(this.getClass());
	
	public void preRender(){
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		if ("true".equals(request.getParameter("error"))) {
			msg.exibirErroGrowl("Denis");
		}
		
	}
	
	public void filtroSpringSecurity() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			ec.dispatch(ec.getRequestContextPath() + "/j_spring_security_check");
			msg.exibirErroGrowl("Denis");
		} catch (IOException e) {
			logger.error("Falha ao executar ação" + this.toString(), e);
		}
		FacesContext.getCurrentInstance().responseComplete();
	}
		
	public void timeout() {
		FacesContext.getCurrentInstance().getExternalContext()
        .invalidateSession();
	}
	
	public void goLogin() {
//		try {
//			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
//			        +"/login.xhtml?timeout=true");
//			FacesContext.getCurrentInstance().responseComplete();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	
}
