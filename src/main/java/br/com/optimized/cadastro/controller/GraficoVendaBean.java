package br.com.optimized.cadastro.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

import br.com.optimized.cadastro.service.VendaService;
import br.com.optimized.config.exception.GlobalException;
import br.com.optimized.seguranca.controller.IdentityBean;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.service.UsuarioService;

@Named
@SessionScoped
public class GraficoVendaBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 609310804088017765L;
	
	private LineChartModel lineModel;
	
	private BarChartModel barModel;

	private static DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM");
//	private static DateFormat DATE_FORMAT_MES = new SimpleDateFormat("MM");
	@Inject
	private IdentityBean identityBean;
	@Inject
	private VendaService vendaService;
	@Inject
	private UsuarioService usuarioService;
	
	@PostConstruct
	public void init() {
		try {
			createLineChart();
			createBarModel();
		} catch (GlobalException e) {
			e.printStackTrace();
		}
	}
	
	 private void createBarModel() throws GlobalException {
    	this.barModel = new BarChartModel();
		this.barModel.setLegendPosition("e");
		this.barModel.setAnimate(true);
		this.barModel.getAxes().put(AxisType.X, new CategoryAxis());
		
		adicionarSerieBar("Meus pedidos", usuarioService.findUsuario(identityBean.getUsuarioLogado().getLogin()));
	 }
	 
	 private void adicionarSerieBar(String rotulo, Usuario criadoPor) {
			Map<Date, BigDecimal> valoresPorData = vendaService.valoresTotaisPorData(15, criadoPor);
			
			ChartSeries series = new ChartSeries(rotulo);
			
			for (Date data : valoresPorData.keySet()) {
				series.set(DATE_FORMAT.format(data), valoresPorData.get(data));
			}
			
			this.barModel.addSeries(series);
		}
	 
	 public String mesAno(String mes) {
		 switch(mes) {
		  case "01":
			  mes = "Janeiro";
		    break;
		  case "02":
			  mes = "Fevereiro";
		    break;
		  case "03":
			  mes = "Março";
		    break;
		  case "04":
			  mes = "Abril";
		    break;
		  case "05":
			  mes = "Maio";
		    break;
		  case "06":
			  mes = "Junho";
		    break;
		  case "07":
			  mes = "Julho";
		    break;
		  case "08":
			  mes = "Agosto";
		    break;
		  case "09":
			  mes = "Setembro";
		    break;
		  case "10":
			  mes = "Outubro";
		    break;
		  case "11":
			  mes = "Novembro";
		    break;
		  case "12":
			  mes = "Dezembro";
		    break;
		  default:
		    // code block
		}
		 return mes;
	 }

	
	public void createLineChart() throws GlobalException {
		this.lineModel = new LineChartModel();
		this.lineModel.setLegendPosition("e");
		this.lineModel.setAnimate(true);
		this.lineModel.getAxes().put(AxisType.X, new CategoryAxis());
		
		adicionarSerie("Todos os pedidos", null);
		adicionarSerie("Meus pedidos", usuarioService.findUsuario(identityBean.getUsuarioLogado().getLogin()));
	}
	
	private void adicionarSerie(String rotulo, Usuario criadoPor) {
		Map<Date, BigDecimal> valoresPorData = vendaService.valoresTotaisPorData(15, criadoPor);
		
		ChartSeries series = new ChartSeries(rotulo);
		
		for (Date data : valoresPorData.keySet()) {
			series.set(DATE_FORMAT.format(data), valoresPorData.get(data));
		}
		this.lineModel.addSeries(series);
	}

	public LineChartModel getLineModel() {
		return lineModel;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

}
