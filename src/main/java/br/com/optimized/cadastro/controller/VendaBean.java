package br.com.optimized.cadastro.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.cadastro.model.Cliente;
import br.com.optimized.cadastro.model.ItemVenda;
import br.com.optimized.cadastro.model.Produto;
import br.com.optimized.cadastro.model.Venda;
import br.com.optimized.cadastro.model.enums.StatusVenda;
import br.com.optimized.cadastro.model.filter.VendaFilter;
import br.com.optimized.cadastro.service.ClienteService;
import br.com.optimized.cadastro.service.ProdutoService;
import br.com.optimized.cadastro.service.VendaService;
import br.com.optimized.config.mail.Mailer;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.generico.controller.AbstractBean;
import br.com.optimized.seguranca.controller.IdentityBean;

@Named
@ConversationScoped
public class VendaBean  extends AbstractBean<VendaService, Venda, Long>{

	private static final long serialVersionUID = 1L;
	
	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "vendaForm.xhtml" + PARAM_REDIRECT;
	private static final String URL_LIST = "vendaList.xhtml" + PARAM_REDIRECT;
	@Inject
	private ClienteService clienteService;
	@Inject
	private ProdutoService produtoService;
	@Inject
	private IdentityBean identityBean;
	@Inject
	private Mailer mailer;
	private Integer quantidade = 1;
	private BigDecimal valorTotal = BigDecimal.ZERO;
	private Produto produtoSelecionado = new Produto();
	private List<ItemVenda> itensVenda = new ArrayList<>();
	private VendaFilter filtro;
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private LazyDataModel<Venda> model;
	
	public VendaBean() {
		filtro = new VendaFilter();
		model = new LazyDataModel<Venda>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Venda> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		setTipo("venda");
		inicializaLista();
	}
	
	public void inicializaLista() {
		try {
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	@Transactional
	public String salvar() {
		if(validaVenda(getSelecionado())){
			return null;
		}else {
		try {
			getSelecionado().setUsuario(identityBean.getUsuarioLogado());
			getSelecionado().getItens().clear();
			getSelecionado().getItens().addAll(itensVenda);
			service.incluir(getSelecionado());
			selecionados = service.todos();
			facesUtils.msgFeedback(null, FacesUtil.msgBundle.getString("msgMergeSucesso"), FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",	FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return null;
		}
		return URL_LIST;
		}
	}
	
	@Transactional
	public String enviarEmail() {
		if(validaVenda(getSelecionado())){
			return null;
		}else {
		try {
			validaVenda(getSelecionado());
			getSelecionado().setStatus(StatusVenda.EMITIDA);
			getSelecionado().setUsuario(identityBean.getUsuarioLogado());
			getSelecionado().getItens().clear();
			getSelecionado().getItens().addAll(itensVenda);
			if(getSelecionado().getId() != null) {
				service.alterar(getSelecionado().getId(), getSelecionado());
			}else {
				service.incluir(getSelecionado());
			}
			alterarQtdProduto(getSelecionado().getItens());
			mailer.enviar(getSelecionado());
			selecionados = service.todos();
			facesUtils.msgFeedback(null, FacesUtil.msgBundle.getString("msgMergeSucesso"), FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",	FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return null;
		}
		return URL_LIST;
		}
	}
	
	public Boolean validaVenda(Venda venda) {
		if(venda.getStatus().equals(StatusVenda.EMITIDA)) {
			facesUtils.msgFeedback(null, "Não é possível alterar pedido emitido!",
					FacesMessage.SEVERITY_ERROR);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public void alterarQtdProduto(List<ItemVenda> itens) {
		itens.forEach(itemVenda -> {
			Produto prod = produtoService.buscarPorId(itemVenda.getProduto().getId());
			prod.setQuantidadeEstoque(prod.getQuantidadeEstoque() - itemVenda.getQuantidade());
			produtoService.alterar(prod.getId(), prod);
		});
	}
	
	public String novo() {
		limpar();
		return URL_FORM;
	}
	
	public void limpar() {
		setProdutoSelecionado(new Produto());
		itensVenda = new ArrayList<>();
		setSelecionado(new Venda());
	}
	
	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public String editar() {
		this.renderizaVisualizar = false;
		itensVenda = new ArrayList<>();
		setSelecionado(service.buscarPorId(getSelecionado().getId()));
		itensVenda.addAll(getSelecionado().getItens());
		return URL_FORM;
	}
	
	public String formVisualizar() {
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
	public List<Cliente> autoCompleteCliente(String cliente) {
		try {
			return this.clienteService.retornarClientePorNome(cliente);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",
					FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return new ArrayList<Cliente>();
		}
	}
	
	public List<Produto> autoCompleteProduto(String produto) {
		try {
			return produtoService.retornarProdutoPelaReferencia(produto);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",
					FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return new ArrayList<Produto>();
		}
	}
	
	public void adicionarProduto() {
		if(produtoService.buscarPorId(produtoSelecionado.getId()).getQuantidadeEstoque() < quantidade) {
			facesUtils.msgFeedback(null, "Produto " + produtoSelecionado.getDescricao() + " possui apenas "
					+ produtoSelecionado.getQuantidadeEstoque() +" item em estoque!",
					FacesMessage.SEVERITY_ERROR);
		}else {
			itensVenda.add(new ItemVenda(quantidade, produtoSelecionado.getValorVenda(), getProdutoSelecionado(), selecionado));
			getSelecionado().setValorTotal(BigDecimal.ZERO);
			itensVenda.forEach(item ->{
				getSelecionado().setValorTotal(getSelecionado().getValorTotal().add(item.getValorUnitario().multiply(BigDecimal.valueOf(item.getQuantidade()))));
			});
			getSelecionado().setValorTotal(getSelecionado().getValorTotal().add(getSelecionado().getValorFrete()).subtract(getSelecionado().getValorDesconto()));
			setProdutoSelecionado(new Produto());
			quantidade = 1;
		}
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public VendaFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(VendaFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Venda> getModel() {
		return model;
	}

	public Boolean getRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(Boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}

	public List<ItemVenda> getItensVenda() {
		return itensVenda;
	}

	public void setItensVenda(List<ItemVenda> itensVenda) {
		this.itensVenda = itensVenda;
	}

}
