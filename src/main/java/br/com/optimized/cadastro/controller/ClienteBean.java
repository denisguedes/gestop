package br.com.optimized.cadastro.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.shaded.json.JSONException;
import org.primefaces.shaded.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.Cidade;
import br.com.optimized.cadastro.model.Cliente;
import br.com.optimized.cadastro.model.Endereco;
import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.model.filter.ClienteFilter;
import br.com.optimized.cadastro.service.CidadeService;
import br.com.optimized.cadastro.service.ClienteService;
import br.com.optimized.cadastro.service.EstadoService;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.generico.controller.AbstractBean;

@Named
@ConversationScoped
public class ClienteBean  extends AbstractBean<ClienteService, Cliente, Long>{

	private static final long serialVersionUID = 1L;
	
	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "clienteForm.xhtml" + PARAM_REDIRECT;
	private static final String URL_LIST = "clienteList.xhtml" + PARAM_REDIRECT;
	
	@Inject 
	private CidadeService cidadeService;
	@Inject 
	private EstadoService estadoService;

	private ClienteFilter filtro;
	private List<Estado> listaEstados = new ArrayList<>();
	private List<Cidade> listaCidades = new ArrayList<>();
	private Boolean renderizaCamposCidadeEstado = Boolean.TRUE;
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private Boolean renderizaCpfCnpj = Boolean.FALSE;
	private LazyDataModel<Cliente> model;
	
	public ClienteBean() {
		filtro = new ClienteFilter();
		model = new LazyDataModel<Cliente>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Cliente> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		setTipo("cliente");
		inicializaLista();
	}
	
	@Transactional
	public String salvar() {
		try {
			if(!StringUtils.isEmpty(getSelecionado().getEndereco().getLogradouro())) {
				buscaCidadeNobanco(getSelecionado());
			 }else {
				 getSelecionado().setEndereco(null);
			 }
			selecionado = service.incluir(getSelecionado());
			selecionados = service.todos();
			facesUtils.msgFeedback(null, FacesUtil.msgBundle.getString("msgMergeSucesso"), FacesMessage.SEVERITY_INFO);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",
					FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return null;
		}
		return URL_LIST;
	}
	
	public void inicializaLista() {
		try {
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	public String buscarCep() throws IOException {
        String json = "";
        URL url = new URL("http://viacep.com.br/ws/"+ getSelecionado().getEndereco().getCep() +"/json");
        URLConnection urlConnection = url.openConnection();
        InputStream is = urlConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        StringBuilder jsonSb = new StringBuilder();

        br.lines().forEach(l -> jsonSb.append(l.trim()));

        json = jsonSb.toString();

        return json;
    }
	
	private void renderizaCamposCidadeEstado(String uf,  String cidade) {
		if(uf.isEmpty() || cidade.isEmpty()) {
			listaEstados();
			this.renderizaCamposCidadeEstado = false;
			PrimeFaces.current().ajax().update("frm:gridEndereco");
		}
	}
	
	public void listaEstados() {
		this.listaEstados = estadoService.listaEstados();
	}
	
	public void buscaCidadeNobanco(Cliente entidade) {
		entidade.getEndereco().getCidade().setNome(this.facesUtils.toUpperCaseSansAccent(entidade.getEndereco().getCidade().getNome()));
		Cidade cidade;
		cidade = cidadeService.listaCidadePorNome(entidade.getEndereco().getCidade());
		entidade.getEndereco().setCidade(cidade);
	}
	
	private void limpaCamposDoEndereco() {
		getSelecionado().getEndereco().setCep("");
		getSelecionado().getEndereco().setLogradouro("");
		getSelecionado().getEndereco().setNumero("");
		getSelecionado().getEndereco().setBairro("");
		getSelecionado().getEndereco().setCidade(new Cidade());
		getSelecionado().getEndereco().getCidade().setEstado(new Estado());
	}
	
	public void encontraCep() {
		try{
			 String json = buscarCep();
			 JSONObject objJson = new JSONObject(json);
			 String uf = objJson.getString("uf");
			 String cidade = objJson.getString("localidade");
			 String logradouro = objJson.getString("logradouro");
			 String bairro = objJson.getString("bairro");
			 
			 renderizaCamposCidadeEstado(uf, cidade);
			 
		     String ufUTF8 = new String(uf.getBytes(), "UTF-8");
		     String cidadeUTF8 = new String(cidade.getBytes(), "UTF-8");
		     String logradouroUTF8 = new String(logradouro.getBytes(), "UTF-8");
		     String bairroUTF8 = new String(bairro.getBytes(), "UTF-8");
			 
		     getSelecionado().getEndereco().setLogradouro(logradouroUTF8);
			 getSelecionado().getEndereco().setBairro(bairroUTF8);
			 if(!uf.isEmpty() && !cidade.isEmpty()) {
				 getSelecionado().getEndereco().getCidade().setNome(cidadeUTF8);
				 getSelecionado().getEndereco().getCidade().getEstado().setSigla(ufUTF8);
			 }
			 getSelecionado().getEndereco().setNumero("");
		}catch (JSONException e) {
			facesUtils.msgFeedback(null , FacesUtil.msgBundle.getString("msgFeedbackUsuarioCepNaoEncontrado") , FacesMessage.SEVERITY_ERROR);
			logger.error(FacesUtil.msgBundle.getString("msgFeedbackUsuarioCepNaoEncontrado"), e);
			PrimeFaces.current().ajax().update("growl");
			limpaCamposDoEndereco();
		} catch (IOException e) {
			facesUtils.msgFeedback(null , FacesUtil.msgBundle.getString("msgFeedbackUsuarioCepDeveSerInformado") , FacesMessage.SEVERITY_ERROR);
			logger.error(FacesUtil.msgBundle.getString("msgFeedbackUsuarioCepDeveSerInformado"), e);
			PrimeFaces.current().ajax().update("growl");
			limpaCamposDoEndereco();
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();

		}
	 }
	
	public String novo() {
		setSelecionado(new Cliente());
		return URL_FORM;
	}
	
	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public void habilitaCampoCpfCnpj() {
		if(selecionado.getTipoPessoa().equals("FISICA")) 
			renderizaCpfCnpj = false;
		else 
			renderizaCpfCnpj = true;
	}
	
	public String editar() {
	   this.renderizaVisualizar = false;
	   setSelecionado(service.buscarPorId(getSelecionado().getId()));
	   if (selecionado != null && selecionado.getEndereco() == null) {
            Cidade cidade = new Cidade();
            Endereco endereco = new Endereco();
            endereco.setCidade(cidade);

            selecionado.setEndereco(endereco);
        }
		return URL_FORM;
	}
	
	public String formVisualizar() {
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public ClienteFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(ClienteFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Cliente> getModel() {
		return model;
	}

	public Boolean getRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(Boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

	public Boolean getRenderizaCpfCnpj() {
		return renderizaCpfCnpj;
	}

	public void setRenderizaCpfCnpj(Boolean renderizaCpfCnpj) {
		this.renderizaCpfCnpj = renderizaCpfCnpj;
	}

	public Boolean getRenderizaCamposCidadeEstado() {
		return renderizaCamposCidadeEstado;
	}

	public void setRenderizaCamposCidadeEstado(Boolean renderizaCamposCidadeEstado) {
		this.renderizaCamposCidadeEstado = renderizaCamposCidadeEstado;
	}

	public List<Estado> getListaEstados() {
		return listaEstados;
	}

	public void setListaEstados(List<Estado> listaEstados) {
		this.listaEstados = listaEstados;
	}

	public List<Cidade> getListaCidades() {
		return listaCidades;
	}

	public void setListaCidades(List<Cidade> listaCidades) {
		this.listaCidades = listaCidades;
	}

}
