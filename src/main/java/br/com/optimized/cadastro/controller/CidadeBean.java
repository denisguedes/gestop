package br.com.optimized.cadastro.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.com.optimized.cadastro.model.Cidade;
import br.com.optimized.cadastro.model.filter.CidadeFilter;
import br.com.optimized.cadastro.service.CidadeService;
import br.com.optimized.generico.controller.AbstractBean;

@Named
@ConversationScoped
public class CidadeBean extends AbstractBean<CidadeService, Cidade, Long>{

	private static final long serialVersionUID = 1L;
	
	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "cidadeForm.xhtml" + PARAM_REDIRECT;
	private static final String URL_LIST = "cidadeList.xhtml" + PARAM_REDIRECT;

	private CidadeFilter filtro;
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private LazyDataModel<Cidade> model;
	
	public CidadeBean() {
		filtro = new CidadeFilter();
		model = new LazyDataModel<Cidade>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		setTipo("cidade");
		inicializaLista();
	}
	
	public void inicializaLista() {
		try {
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	public String novo() {
		setSelecionado(new Cidade());
		return URL_FORM;
	}
	
	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public String editar() {
		this.renderizaVisualizar = false;
		setSelecionado(service.buscarPorId(getSelecionado().getId()));
		return URL_FORM;
	}
	
	public String formVisualizar() {
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public CidadeFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(CidadeFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Cidade> getModel() {
		return model;
	}

	public Boolean getRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(Boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

}
