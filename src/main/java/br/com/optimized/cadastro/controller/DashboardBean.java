package br.com.optimized.cadastro.controller;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.optimized.cadastro.model.dto.ValorItensEstoque;
import br.com.optimized.cadastro.service.ClienteService;
import br.com.optimized.cadastro.service.ProdutoService;
import br.com.optimized.cadastro.service.VendaService;

@Named
@SessionScoped
public class DashboardBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 609310804088017765L;
	
	@Inject
	private VendaService vendaService;
	@Inject
	private ProdutoService produtoService;
	@Inject
	private ClienteService clienteService;
		
	private BigDecimal vendasNoAno;
	
	private BigDecimal vendasNoMes;

	private BigDecimal ticketMedio;
	
	private Long totalClientes;
	
	private ValorItensEstoque valorItensEstoque;
	
	@PostConstruct
	public void init() {
		this.vendasNoAno = vendaService.valorTotalNoAno();
		this.vendasNoMes = vendaService.valorTotalNoMes();
		this.ticketMedio = vendaService.valorTicketMedioNoAno();
		
		this.valorItensEstoque = produtoService.valorItensEstoque();
		this.totalClientes = clienteService.quantidade();
	}

	public BigDecimal getVendasNoAno() {
		return vendasNoAno;
	}

	public void setVendasNoAno(BigDecimal vendasNoAno) {
		this.vendasNoAno = vendasNoAno;
	}

	public BigDecimal getVendasNoMes() {
		return vendasNoMes;
	}

	public void setVendasNoMes(BigDecimal vendasNoMes) {
		this.vendasNoMes = vendasNoMes;
	}

	public BigDecimal getTicketMedio() {
		return ticketMedio;
	}

	public void setTicketMedio(BigDecimal ticketMedio) {
		this.ticketMedio = ticketMedio;
	}

	public Long getTotalClientes() {
		return totalClientes;
	}

	public void setTotalClientes(Long totalClientes) {
		this.totalClientes = totalClientes;
	}

	public ValorItensEstoque getValorItensEstoque() {
		return valorItensEstoque;
	}

	public void setValorItensEstoque(ValorItensEstoque valorItensEstoque) {
		this.valorItensEstoque = valorItensEstoque;
	}

}
