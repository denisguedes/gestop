package br.com.optimized.cadastro.controller;

import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.file.UploadedFile;

import br.com.optimized.cadastro.model.Categoria;
import br.com.optimized.cadastro.model.Fornecedor;
import br.com.optimized.cadastro.model.Produto;
import br.com.optimized.cadastro.model.enums.TipoProduto;
import br.com.optimized.cadastro.model.filter.ProdutoFilter;
import br.com.optimized.cadastro.service.CategoriaService;
import br.com.optimized.cadastro.service.FornecedorService;
import br.com.optimized.cadastro.service.ProdutoService;
import br.com.optimized.config.util.FilesUtils;
import br.com.optimized.generico.controller.AbstractBean;

@Named
@ConversationScoped
public class ProdutoBean  extends AbstractBean<ProdutoService, Produto, Long>{

	private static final long serialVersionUID = 1L;
	
	private static final String PARAM_REDIRECT = "?faces-redirect=true";
	private static final String URL_FORM = "produtoForm.xhtml" + PARAM_REDIRECT;
	private static final String URL_LIST = "produtoList.xhtml" + PARAM_REDIRECT;

	@Inject
	private CategoriaService categoriaService;
	@Inject
	private FornecedorService fornecedorService;
	private List<Categoria> categorias = new ArrayList<>();
	private Set<TipoProduto> tipoProduto;
	private ProdutoFilter filtro;
	private Boolean renderizaVisualizar = Boolean.FALSE;
	private LazyDataModel<Produto> model;
	
	public ProdutoBean() {
		filtro = new ProdutoFilter();
		model = new LazyDataModel<Produto>() {

			private static final long serialVersionUID = 1L;
			
			@Override
			public List<Produto> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,FilterMeta> filterBy)  {
				
				filtro.setPrimeiroRegistro(first);
				filtro.setQuantidadeRegistros(pageSize);
				filtro.setPropriedadeOrdenacao(sortField);
				filtro.setAscendente(SortOrder.ASCENDING.equals(sortOrder));
				setRowCount(service.quantidadeFiltrados(filtro).intValue());
				return service.pesquisar(filtro);
			}
		};
	}
	
	@PostConstruct
	public void init() {
		setTipo("produto");
		 tipoProduto = new HashSet<>();
			for(TipoProduto tipo : TipoProduto.values()) {
				tipoProduto.add(tipo);
			}
		categorias = categoriaService.todos();
		inicializaLista();
	}
	
	public void inicializaLista() {
		try {
			selecionados = service.pesquisar(filtro);
		} catch (Exception e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
		}
	}
	
	public List<Fornecedor> autoCompleteFornecedor(String nomeFantasia) {
		try {
			return this.fornecedorService.retornarFornecedorPorNomeFantasia(nomeFantasia);
		} catch (Exception e) {
			facesUtils.msgFeedback(null, "Falha ao executar ação",
					FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			return new ArrayList<Fornecedor>();
		}
	}
	
	public String novo() {
		setSelecionado(new Produto());
		return URL_FORM;
	}
	
	public String retornarPesquisa() {
		endConversation();
		return URL_LIST;
	}
	
	public String editar() {
		this.renderizaVisualizar = false;
		setSelecionado(service.buscarPorId(getSelecionado().getId()));
		return URL_FORM;
	}
	
	public String formVisualizar() {
		this.renderizaVisualizar = true;
		return URL_FORM;
	}
	
    /**
     * Faz um upload do arquivo de imagens.
     * @param event arquivo uploadeado
     */
    public void uploadImagem(FileUploadEvent event) {

        UploadedFile item = event.getFile();
        if (item == null) {
            return;
        }

        String extension = FilesUtils.getExtension(item.getFileName());
        String name = FilesUtils.getNewFileName(item.getFileName());
        String fileName = name + "." + extension;
        String folderName = "/arquivo/foto-produto/";

        try {
            // Copia a imagem original e cria uma miniatura.
            FilesUtils.salvarImagem(item.getContent(), new File(getPath(folderName).replace("gestop", "")), name, extension);
            getSelecionado().setFoto(folderName + fileName);

        } catch (IllegalArgumentException e) {
			facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			e.printStackTrace();
			} catch (ImagingOpException e) {
				facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
				logger.error("Falha ao executar ação", e);
				e.printStackTrace();
			} catch (IOException e) {
				facesUtils.msgFeedback(null , "Falha ao executar ação" , FacesMessage.SEVERITY_ERROR);
				logger.error("Falha ao executar ação", e);
				e.printStackTrace();
			}
    }

    public void removerImagem() {
        getSelecionado().setFoto(null);
    }

    /**
     * Pega o caminho real.
     * @param caminho caminho
     * @return caminho real
     */
    protected String getPath(String caminho) {
        return FilesUtils.getRealPathEmpty() + caminho;
    }
    
	public boolean isEditando() {
		return getSelecionado().getId() != null;
	}

	public ProdutoFilter getFiltro() {
		return filtro;
	}

	public void setFiltro(ProdutoFilter filtro) {
		this.filtro = filtro;
	}

	public LazyDataModel<Produto> getModel() {
		return model;
	}

	public Boolean getRenderizaVisualizar() {
		return renderizaVisualizar;
	}

	public void setRenderizaVisualizar(Boolean renderizaVisualizar) {
		this.renderizaVisualizar = renderizaVisualizar;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Set<TipoProduto> getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(Set<TipoProduto> tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

}
