package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Produto;
import br.com.optimized.cadastro.model.dto.ValorItensEstoque;
import br.com.optimized.cadastro.model.filter.ProdutoFilter;

public interface ProdutoRepositoryQuery {

	public List<Produto> pesquisar(ProdutoFilter filtro);
	
	public Long total(ProdutoFilter filtro);	
	
	public ValorItensEstoque valorItensEstoque();
	
	public List<Produto> retornarProdutoPelaReferencia(String produto);
	
}
