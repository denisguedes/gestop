package br.com.optimized.cadastro.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.Cidade;
import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.model.filter.CidadeFilter;
import br.com.optimized.cadastro.repository.query.CidadeRepositoryQuery;

public class CidadeRepositoryImpl implements CidadeRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<Cidade> pesquisar(CidadeFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Cidade> criteria = builder.createQuery(Cidade.class);
		Root<Cidade> root = criteria.from(Cidade.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Cidade> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(CidadeFilter filtro, CriteriaBuilder builder,
			Root<Cidade> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}	
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, CidadeFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(CidadeFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Cidade> root = criteria.from(Cidade.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	public List<Cidade> listaCidadesPorCodigoEstado(Estado estado) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" SELECT cid FROM Cidade cid ");
		jpql.append(" INNER JOIN cid.estado estado ");
		jpql.append(" WHERE cid.estado.id = :estadoId ");

		TypedQuery<Cidade> query = manager.createQuery(jpql.toString(), Cidade.class);
		query.setParameter("estadoId", estado.getId());

		return query.getResultList();
	}

	public Cidade listaCidadePorNome(Cidade cidade) {
		try {
			StringBuffer jpql = new StringBuffer();
			jpql.append(" SELECT cid FROM Cidade cid ");
			jpql.append(" WHERE cid.nome = :nome AND cid.estado.sigla = :uf ");

			TypedQuery<Cidade> query = manager.createQuery(jpql.toString(), Cidade.class);
			query.setParameter("nome", cidade.getNome().toUpperCase().replace("'", " "));
			query.setParameter("uf", cidade.getEstado().getSigla());
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
