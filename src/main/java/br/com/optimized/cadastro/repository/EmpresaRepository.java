package br.com.optimized.cadastro.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Empresa;
import br.com.optimized.cadastro.repository.query.EmpresaRepositoryQuery;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long>, EmpresaRepositoryQuery {

	List<Empresa> findAllByOrderByNomeFantasia();
	
	public Optional<Empresa> findByCnpj(String cpfOuCnpj);

	public List<Empresa> findByNomeFantasiaStartingWithIgnoreCase(String nome);
	
}
