package br.com.optimized.cadastro.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.Empresa;
import br.com.optimized.cadastro.model.filter.EmpresaFilter;
import br.com.optimized.cadastro.repository.query.EmpresaRepositoryQuery;

public class EmpresaRepositoryImpl implements EmpresaRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<Empresa> pesquisar(EmpresaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Empresa> criteria = builder.createQuery(Empresa.class);
		Root<Empresa> root = criteria.from(Empresa.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nomeFantasia")));

		TypedQuery<Empresa> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(EmpresaFilter filtro, CriteriaBuilder builder,
			Root<Empresa> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNomeFantasia())) {
			predicates.add(builder.like(builder.lower(root.get("nomeFantasia")),
					"%" + filtro.getNomeFantasia().toLowerCase() + "%"));
		}	
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, EmpresaFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(EmpresaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Empresa> root = criteria.from(Empresa.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	public List<Empresa> retornarEmpresaPorNomeFantasia(String nomeFantasia){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Empresa> criteria = builder.createQuery(Empresa.class);
		Root<Empresa> root = criteria.from(Empresa.class);

		criteria.where(builder.like(builder.lower(root.get("nomeFantasia")),
				"%" + nomeFantasia + "%"));
		criteria.orderBy(builder.asc(root.get("nomeFantasia")));
	
		TypedQuery<Empresa> query = manager.createQuery(criteria);

		return query.getResultList();
	}

}
