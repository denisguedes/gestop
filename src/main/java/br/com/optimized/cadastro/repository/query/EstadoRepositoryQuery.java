package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.model.filter.EstadoFilter;

public interface EstadoRepositoryQuery {

	public List<Estado> pesquisar(EstadoFilter filtro);
	
	public Long total(EstadoFilter filtro);	
}
