package br.com.optimized.cadastro.repository.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import br.com.optimized.cadastro.model.Email;
import br.com.optimized.cadastro.model.filter.EmailFilter;
import br.com.optimized.cadastro.repository.query.EmailRepositoryQuery;
import br.com.optimized.config.util.EntityManagerProducer;

public class EmailRepositoryImpl implements EmailRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	public Email retornarEmailSMTP() {
		try {
			manager = EntityManagerProducer.createEntityManager();
			StringBuffer jpql = new StringBuffer();
			jpql.append(" SELECT e FROM Email e ");
			jpql.append(" WHERE e.id = (SELECT MAX(ei.id) FROM Email ei) ");
	
			TypedQuery<Email> query = manager.createQuery(jpql.toString() , Email.class);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} finally {
			manager.close();
		}
	}

	@Override
	public List<Email> pesquisar(EmailFilter filtro) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long total(EmailFilter filtro) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
