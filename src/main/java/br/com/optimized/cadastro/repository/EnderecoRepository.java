package br.com.optimized.cadastro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Endereco;
import br.com.optimized.cadastro.repository.query.EnderecoRepositoryQuery;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long>, EnderecoRepositoryQuery {

	List<Endereco> findAllByOrderByNome();
	
}
