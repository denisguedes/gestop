package br.com.optimized.cadastro.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.ParametroSistema;
import br.com.optimized.cadastro.model.filter.ParametroSistemaFilter;
import br.com.optimized.cadastro.repository.query.ParametroSistemaRepositoryQuery;

public class ParametroSistemaRepositoryImpl implements ParametroSistemaRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<ParametroSistema> pesquisar(ParametroSistemaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ParametroSistema> criteria = builder.createQuery(ParametroSistema.class);
		Root<ParametroSistema> root = criteria.from(ParametroSistema.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<ParametroSistema> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(ParametroSistemaFilter filtro, CriteriaBuilder builder,
			Root<ParametroSistema> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}	
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, ParametroSistemaFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(ParametroSistemaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ParametroSistema> root = criteria.from(ParametroSistema.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	public ParametroSistema buscarPorNome(String nome) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" SELECT p FROM ParametroSistema p ");
		jpql.append(" WHERE p.nome = :nome ");

		TypedQuery<ParametroSistema> query = manager.createQuery(jpql.toString(), ParametroSistema.class);
		query.setParameter("nome", nome);

		ParametroSistema parametroSistema = query.getSingleResult();

		return parametroSistema;
	}
	
	public String retornaTempoJobApuracao(String nome) {
		StringBuffer jpql = new StringBuffer();
		jpql.append(" select p.valor from ParametroSistema p where upper(p.nome) = upper(:nome) ");
		TypedQuery<String> query = manager.createQuery(jpql.toString(), String.class);
		query.setParameter("nome", nome);
		return query.getSingleResult();
	}
	
	public Boolean isFiltroProdutoEbsAtivo() {
		StringBuffer jpql = new StringBuffer(" select count(p) from ParametroSistema p where upper(p.nome) = 'STATUS_FILTRO_PRODUTO' and p.valor = 'true' ");
		TypedQuery<Integer> query = manager.createQuery(jpql.toString(), Integer.class);
		return query.getSingleResult() > 0;		
	}

}