package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.ParametroSistema;
import br.com.optimized.cadastro.model.filter.ParametroSistemaFilter;

public interface ParametroSistemaRepositoryQuery {

	public List<ParametroSistema> pesquisar(ParametroSistemaFilter filtro);
	
	public Long total(ParametroSistemaFilter filtro);	
}
