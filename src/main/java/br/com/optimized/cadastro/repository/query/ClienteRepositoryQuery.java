package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Cliente;
import br.com.optimized.cadastro.model.filter.ClienteFilter;

public interface ClienteRepositoryQuery {

	public List<Cliente> pesquisar(ClienteFilter filtro);
	
	public Long total(ClienteFilter filtro);	
	
	public List<Cliente> retornarClientePorNome(String cliente);
}
