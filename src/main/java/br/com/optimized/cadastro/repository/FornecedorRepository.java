package br.com.optimized.cadastro.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Fornecedor;
import br.com.optimized.cadastro.repository.query.FornecedorRepositoryQuery;

@Repository
public interface FornecedorRepository extends JpaRepository<Fornecedor, Long>, FornecedorRepositoryQuery {

	List<Fornecedor> findAllByOrderByNomeFantasia();
	
	public Optional<Fornecedor> findByCnpj(String cpfOuCnpj);

	public List<Fornecedor> findByNomeFantasiaStartingWithIgnoreCase(String nome);
	
}
