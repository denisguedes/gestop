package br.com.optimized.cadastro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.repository.query.EstadoRepositoryQuery;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>, EstadoRepositoryQuery {

	List<Estado> findAllByOrderByNome();
	
}
