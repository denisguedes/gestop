package br.com.optimized.cadastro.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.Produto;
import br.com.optimized.cadastro.model.dto.ValorItensEstoque;
import br.com.optimized.cadastro.model.filter.ProdutoFilter;
import br.com.optimized.cadastro.repository.query.ProdutoRepositoryQuery;

public class ProdutoRepositoryImpl implements ProdutoRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<Produto> pesquisar(ProdutoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Produto> criteria = builder.createQuery(Produto.class);
		Root<Produto> root = criteria.from(Produto.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Produto> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(ProdutoFilter filtro, CriteriaBuilder builder,
			Root<Produto> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}
		if (!StringUtils.isEmpty(filtro.getSku())) {
			predicates.add(builder.like(builder.lower(root.get("sku")),
					"%" + filtro.getSku().toLowerCase() + "%"));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, ProdutoFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(ProdutoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Produto> root = criteria.from(Produto.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public ValorItensEstoque valorItensEstoque() {
		String query = "select new br.com.optimized.cadastro.model.dto.ValorItensEstoque(sum(valorVenda * quantidadeEstoque), sum(quantidadeEstoque)) from Produto";
		return manager.createQuery(query, ValorItensEstoque.class).getSingleResult();
	}
	
	public List<Produto> retornarProdutoPelaReferencia(String produto){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Produto> criteria = builder.createQuery(Produto.class);
		Root<Produto> root = criteria.from(Produto.class);

		criteria.where(builder.like(root.get("referencia"),
				"%" + produto + "%"));
		criteria.orderBy(builder.asc(root.get("nome")));
	
		TypedQuery<Produto> query = manager.createQuery(criteria);

		return query.getResultList();
	}
	
}
