package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Fornecedor;
import br.com.optimized.cadastro.model.filter.FornecedorFilter;

public interface FornecedorRepositoryQuery {

	public List<Fornecedor> pesquisar(FornecedorFilter filtro);
	
	public Long total(FornecedorFilter filtro);	
	
	public List<Fornecedor> retornarFornecedorPorNomeFantasia(String nomeFantasia);
}
