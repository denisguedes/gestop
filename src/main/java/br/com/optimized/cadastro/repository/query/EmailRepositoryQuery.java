package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Email;
import br.com.optimized.cadastro.model.filter.EmailFilter;

public interface EmailRepositoryQuery {

	public List<Email> pesquisar(EmailFilter filtro);
	
	public Long total(EmailFilter filtro);	
	
	public Email retornarEmailSMTP();
}
