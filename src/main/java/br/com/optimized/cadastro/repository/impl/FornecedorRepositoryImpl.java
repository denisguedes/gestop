package br.com.optimized.cadastro.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.Fornecedor;
import br.com.optimized.cadastro.model.filter.FornecedorFilter;
import br.com.optimized.cadastro.repository.query.FornecedorRepositoryQuery;

public class FornecedorRepositoryImpl implements FornecedorRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<Fornecedor> pesquisar(FornecedorFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Fornecedor> criteria = builder.createQuery(Fornecedor.class);
		Root<Fornecedor> root = criteria.from(Fornecedor.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nomeFantasia")));

		TypedQuery<Fornecedor> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(FornecedorFilter filtro, CriteriaBuilder builder,
			Root<Fornecedor> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNomeFantasia())) {
			predicates.add(builder.like(builder.lower(root.get("nomeFantasia")),
					"%" + filtro.getNomeFantasia().toLowerCase() + "%"));
		}	
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, FornecedorFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(FornecedorFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Fornecedor> root = criteria.from(Fornecedor.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	public List<Fornecedor> retornarFornecedorPorNomeFantasia(String nomeFantasia){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Fornecedor> criteria = builder.createQuery(Fornecedor.class);
		Root<Fornecedor> root = criteria.from(Fornecedor.class);

		criteria.where(builder.like(root.get("nomeFantasia"),
				"%" + nomeFantasia + "%"));
		criteria.orderBy(builder.asc(root.get("nomeFantasia")));
	
		TypedQuery<Fornecedor> query = manager.createQuery(criteria);

		return query.getResultList();
	}

}
