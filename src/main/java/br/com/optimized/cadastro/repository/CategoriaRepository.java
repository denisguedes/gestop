package br.com.optimized.cadastro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Categoria;
import br.com.optimized.cadastro.repository.query.CategoriaRepositoryQuery;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long>, CategoriaRepositoryQuery {

	List<Categoria> findAllByOrderByDescricao();

}
