package br.com.optimized.cadastro.repository.query;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.optimized.cadastro.model.Venda;
import br.com.optimized.cadastro.model.dto.VendaMes;
import br.com.optimized.cadastro.model.dto.VendaOrigem;
import br.com.optimized.cadastro.model.filter.VendaFilter;
import br.com.optimized.seguranca.model.Usuario;

public interface VendaRepositoryQuery {

	public List<Venda> pesquisar(VendaFilter filtro);
	
	public Long total(VendaFilter filtro);	
	
	public Venda buscarComItens(Long codigo);
	
	public BigDecimal valorTotalNoAno();
	
	public BigDecimal valorTotalNoMes();
	
	public BigDecimal valorTicketMedioNoAno();
	
	public List<VendaMes> totalPorMes();
	
	public List<VendaOrigem> totalPorOrigem();
	
	public Map<Date, BigDecimal> valoresTotaisPorData(Integer numeroDeDias, Usuario criadoPor);

	public Map<Date, BigDecimal> valoresTotaisPorDataBar(Integer numeroDeDias, Usuario criadoPor);
	
}
