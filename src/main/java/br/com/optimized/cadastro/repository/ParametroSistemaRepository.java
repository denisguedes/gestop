package br.com.optimized.cadastro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.ParametroSistema;
import br.com.optimized.cadastro.repository.query.ParametroSistemaRepositoryQuery;

@Repository
public interface ParametroSistemaRepository extends JpaRepository<ParametroSistema, Long>, ParametroSistemaRepositoryQuery {

	List<ParametroSistema> findAllByOrderByNome();
	
}
