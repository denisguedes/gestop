package br.com.optimized.cadastro.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.util.StringUtils;

import br.com.optimized.cadastro.model.Cliente;
import br.com.optimized.cadastro.model.filter.ClienteFilter;
import br.com.optimized.cadastro.repository.query.ClienteRepositoryQuery;

public class ClienteRepositoryImpl implements ClienteRepositoryQuery {

	@Inject
	private EntityManager manager;
	
	@Override
	public List<Cliente> pesquisar(ClienteFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Cliente> criteria = builder.createQuery(Cliente.class);
		Root<Cliente> root = criteria.from(Cliente.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Cliente> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, filtro);

		return query.getResultList();
	}

	private Predicate[] criarRestricoes(ClienteFilter filtro, CriteriaBuilder builder,
			Root<Cliente> root) {

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}	
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, ClienteFilter filtro) {
		query.setFirstResult(filtro.getPrimeiroRegistro());
		query.setMaxResults(filtro.getQuantidadeRegistros());
	}

	public Long total(ClienteFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Cliente> root = criteria.from(Cliente.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	public List<Cliente> retornarClientePorNome(String cliente){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Cliente> criteria = builder.createQuery(Cliente.class);
		Root<Cliente> root = criteria.from(Cliente.class);

		criteria.where(builder.like(root.get("nome"),
				"%" + cliente + "%"));
		criteria.orderBy(builder.asc(root.get("nome")));
	
		TypedQuery<Cliente> query = manager.createQuery(criteria);

		return query.getResultList();
	}

}
