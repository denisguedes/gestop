package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Endereco;
import br.com.optimized.cadastro.model.filter.EnderecoFilter;

public interface EnderecoRepositoryQuery {

	public List<Endereco> pesquisar(EnderecoFilter filtro);
	
	public Long total(EnderecoFilter filtro);	
}
