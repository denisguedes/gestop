package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Empresa;
import br.com.optimized.cadastro.model.filter.EmpresaFilter;

public interface EmpresaRepositoryQuery {

	public List<Empresa> pesquisar(EmpresaFilter filtro);
	
	public Long total(EmpresaFilter filtro);	
	
	public List<Empresa> retornarEmpresaPorNomeFantasia(String nomeFantasia);
}
