package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Categoria;
import br.com.optimized.cadastro.model.filter.CategoriaFilter;

public interface CategoriaRepositoryQuery {
	
	public List<Categoria> pesquisar(CategoriaFilter filtro);
	
	public Long total(CategoriaFilter filtro);		
}
