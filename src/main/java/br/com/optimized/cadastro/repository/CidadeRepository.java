package br.com.optimized.cadastro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Cidade;
import br.com.optimized.cadastro.repository.query.CidadeRepositoryQuery;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long>, CidadeRepositoryQuery {

	List<Cidade> findAllByOrderByNome();
	
}
