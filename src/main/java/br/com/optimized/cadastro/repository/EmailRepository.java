package br.com.optimized.cadastro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.optimized.cadastro.model.Email;
import br.com.optimized.cadastro.repository.query.EmailRepositoryQuery;

@Repository
public interface EmailRepository extends JpaRepository<Email, Long>, EmailRepositoryQuery {

	List<Email> findAllByOrderByNome();
	
}
