package br.com.optimized.cadastro.repository.query;

import java.util.List;

import br.com.optimized.cadastro.model.Cidade;
import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.model.filter.CidadeFilter;

public interface CidadeRepositoryQuery {

	public List<Cidade> pesquisar(CidadeFilter filtro);
	
	public Long total(CidadeFilter filtro);
	
	public List<Cidade> listaCidadesPorCodigoEstado(Estado estado);
	
	public Cidade listaCidadePorNome(Cidade cidade);

}
