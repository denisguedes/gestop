package br.com.optimized.cadastro.model.enums;

public enum ParametroSistemaEnum {

	TEMPO_TIMEOUT(), //
	TENTATIVAS_ACESSO(), //
	TEMPO_BLOQUEIO(), //
	HABILITA_CAPTCHA(), //
	TEMPO_SCHEDULE_APURACAO(), //
	STATUS_SCHEDULE_APURACAO(), //
	DATA_INTEGRACAO_WEBSERVICE_CLIENTE(),//
	TEMPO_SCHEDULE_NOTA_FISCAL(),
	ENVIO_EMAIL_PERCENTUAL_VERBA_CRESCIMENTO(),
	ENVIO_EMAIL_PERCENTUAL_VERBA_ATING_META(),
	STATUS_FILTRO_PRODUTO();

	private ParametroSistemaEnum() {
	}

	private ParametroSistemaEnum(String descricao, String valor) {
		this.descricao = descricao;
		this.valor = valor;
	}

	private String descricao;
	private String valor;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Integer intValue() {
		Integer value = 0;
		if(valor != null) {
			value = Integer.parseInt(valor);
		}
		return value;
	}

	public Boolean booleanValue() {
		try {
			return intValue() == 1;
		} catch (NumberFormatException e) {
			return valor.equals("true");
		}
	}

}

