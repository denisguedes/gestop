package br.com.optimized.cadastro.model.filter;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.optimized.cadastro.model.Categoria;
import br.com.optimized.cadastro.model.enums.Origem;
import br.com.optimized.cadastro.model.enums.TipoProduto;
import br.com.optimized.generico.model.filter.FiltroPesquisa;

public class ProdutoFilter extends FiltroPesquisa<Object> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9022087285796846813L;

	private String sku;
	private String nome;
	private Categoria categoria;
	private TipoProduto tipoProduto;
	private Origem origem;
	private BigDecimal valorDe;
	private BigDecimal valorAte;

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public Origem getOrigem() {
		return origem;
	}

	public void setOrigem(Origem origem) {
		this.origem = origem;
	}

	public BigDecimal getValorDe() {
		return valorDe;
	}

	public void setValorDe(BigDecimal valorDe) {
		this.valorDe = valorDe;
	}

	public BigDecimal getValorAte() {
		return valorAte;
	}

	public void setValorAte(BigDecimal valorAte) {
		this.valorAte = valorAte;
	}

}
