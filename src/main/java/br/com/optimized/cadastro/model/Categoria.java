package br.com.optimized.cadastro.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "categoria", schema = "gestop")
public class Categoria extends AbstractModel<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3317155672401690445L;

	@NotBlank(message = "O nome é obrigatório")
	@Size(max = 50, message = "O tamanho da descrição não pode ser maior que {max} caracteres")
	private String descricao;
	
	@JsonManagedReference("produtos")
	@OneToMany(mappedBy = "categoria", cascade = { CascadeType.ALL }, orphanRemoval = true)
	private List<Produto> produtos = new ArrayList<>();

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

}
