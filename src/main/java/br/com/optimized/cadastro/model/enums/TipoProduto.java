package br.com.optimized.cadastro.model.enums;

public enum TipoProduto {

	ACO("Aço"),
	ACESSORIO("Acessório"),
	FOLHEADO("Folheado"),
	OURO("Ouro"),
	PRATA("Prata");
	
	private String descricao;
	
	TipoProduto(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
