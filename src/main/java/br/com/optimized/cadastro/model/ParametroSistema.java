package br.com.optimized.cadastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "parametro_sistema", schema = "gestop")
public class ParametroSistema extends AbstractModel<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1431168920610512019L;

	public ParametroSistema() {
	}
	
	public ParametroSistema(String nome, String descricao, String valor) {
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
	}

	@Column(name = "nome", unique = true)
	private String nome;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "valor")
	private String valor;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
