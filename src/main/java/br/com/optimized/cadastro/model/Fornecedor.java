package br.com.optimized.cadastro.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "fornecedor", schema = "gestop")
public class Fornecedor extends AbstractModel<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5770756980448346164L;

	@Size.List ({
	    @Size(max=10, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_CODIGO_EMPRESA)
	})
	@Column(name = "codigo")
	private String codigo;

	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_NOME_FANTASIA_EMPRESA),
	    @Size(max=120, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NOME_FANTASIA_EMPRESA)
	})
	@Column(name = "nome_fantasia")
	private String nomeFantasia;

	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_RAZAO_SOCIAL_EMPRESA),
	    @Size(max=120, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_RAZAO_SOCIAL_EMPRESA)
	})
	@Column(name = "razao_social")
	private String razaoSocial;
	
	private String telefone;

	@CNPJ
	@Column(name = "cnpj", length = 18)
	private String cnpj;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "endereco_id", referencedColumnName = "id")
	private Endereco endereco;
	
	@Size.List ({
	    @Size(max=20, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_INSCRICAO_ESTADUAL_EMPRESA) 
	})
	@Column(name = "inscricao_estadual")
	private String inscricaoEstadual;
	
	@Size.List ({
	    @Size(max=20, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_INSCRICAO_MUNICIPAL_EMPRESA)
	})
	@Column(name = "inscricao_municipal")
	private String inscricaoMunicipal;
	
	public Fornecedor() {
		this.endereco = new Endereco();
	}

	public Fornecedor(Long id) {
		this.id = id;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		if (inscricaoEstadual.trim().equals("")){
			inscricaoEstadual = null;
		}
		String novoInscricaoEstadual = inscricaoEstadual;
		this.inscricaoEstadual = novoInscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		if (inscricaoMunicipal.trim().equals("")){
			inscricaoMunicipal = null;
		}
		String novoInscricaoEstadual = inscricaoMunicipal;
		this.inscricaoMunicipal = novoInscricaoEstadual;
	}

}