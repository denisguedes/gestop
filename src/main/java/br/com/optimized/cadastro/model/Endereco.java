package br.com.optimized.cadastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "endereco", schema = "gestop")
public class Endereco extends AbstractModel<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 319466721441021072L;

	@Size.List ({	    
	    @Size(max=200, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_LOGRADOURO_ENDERECO)
	})
	@Column(name = "logradouro")
	private String logradouro;
	
	@Column(name = "numero", length = 6)
	private String numero;
	
	@Size.List ({
	    @Size(max=200, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_COMPLEMENTO_ENDERECO)
	})
	@Column(name = "complemento")
	private String complemento;
	
	@Column(name = "bairro")
	private String bairro;
	
	@Column(name = "cep", length = 8)
	private String cep;
	
	@ManyToOne
	@JoinColumn(name = "cidade_id", foreignKey = @ForeignKey(name="cidade_endereco_fk"), referencedColumnName = "id")
	private Cidade cidade;
	
	public Endereco() {
		this.cidade = new Cidade();
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		String novoCep = cep.replace( ".","").replace( "-","");
		
		this.cep = novoCep;
	}
		
}
