package br.com.optimized.cadastro.model.filter;

import java.io.Serializable;

import br.com.optimized.generico.model.filter.FiltroPesquisa;

public class FornecedorFilter extends FiltroPesquisa<Object> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9022087285796846813L;

	private Long id;
	private Long codigo;
	private String nomeFantasia;
	private String razaoSocial;
	private String cpfCnpj;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

}
