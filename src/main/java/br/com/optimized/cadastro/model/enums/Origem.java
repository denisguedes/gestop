package br.com.optimized.cadastro.model.enums;

public enum Origem {

	NACIONAL("Nacional"),
	INTERNACIONAL("Importada");
	
	private String descricao;
	
	Origem(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() { 
		return descricao;
	}
	
}
