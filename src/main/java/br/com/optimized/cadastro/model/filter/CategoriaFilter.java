package br.com.optimized.cadastro.model.filter;

import java.io.Serializable;

import br.com.optimized.generico.model.filter.FiltroPesquisa;

public class CategoriaFilter extends FiltroPesquisa<Object> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9022087285796846813L;

	private Long id;
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
