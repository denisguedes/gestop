/**
 * 
 */
package br.com.optimized.cadastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "cidade", schema = "gestop")
public class Cidade extends AbstractModel<Long>  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5187979962991794921L;

	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_CIDADE_CIDADE),
	    @Size(max=60, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_CIDADE_CIDADE)
	})
	@Column(name = "nome")
	private String nome;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "estado_id", foreignKey = @ForeignKey(name="estado_cidade_fk"), referencedColumnName = "id")
	private Estado estado;
	
	public Cidade() {
		this.estado = new Estado();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
}
