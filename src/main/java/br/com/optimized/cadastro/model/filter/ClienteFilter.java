package br.com.optimized.cadastro.model.filter;

import java.io.Serializable;

import br.com.optimized.cadastro.model.enums.TipoPessoa;
import br.com.optimized.generico.model.filter.FiltroPesquisa;

public class ClienteFilter extends FiltroPesquisa<Object> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9022087285796846813L;

	private String nome;
	private String cpfOuCnpj;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public Object getCpfOuCnpjSemFormatacao() {
		return TipoPessoa.removerFormatacao(this.cpfOuCnpj);
	}

}
