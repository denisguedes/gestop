package br.com.optimized.cadastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "email", schema = "gestop")
public class Email extends AbstractModel<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -285065525053891296L;

	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_HOST_SMTP),
	    @Size(max=40, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_HOST_SMTP)
	})
	@Column(name = "servidor_smtp")
	private String servidorSMTP;
	
	@NotNull
	@Column(name = "porta_smtp")
	private Integer portaSMTP;
	
	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_EMAIL_SMTP),
	    @Size(max=120, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_EMAIL_SMTP)
	})
	@Column(name = "email_from")
	private String emailFrom;
	
	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_USUARIO_EMAIL_SMTP),
	    @Size(max=120, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_USUARIO_EMAIL_SMTP)
	})
	@Column(name = "email_from_usuario")
	private String emailFromUsuario;
	
	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_SENHA_EMAIL_SMTP),
	    @Size(max=20, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_SENHA_EMAIL_SMTP)
	})
	@Column(name = "email_from_senha")
	private String emailFromSenha;
	
	@NotNull
	@Column(name = "criptografado")
	private Boolean criptografado;
	
	
	public Email() {
		super();
		this.criptografado = true;
	}

	public String getServidorSMTP() {
		return servidorSMTP;
	}

	public void setServidorSMTP(String servidorSMTP) {
		this.servidorSMTP = servidorSMTP;
	}

	public Integer getPortaSMTP() {
		return portaSMTP;
	}

	public void setPortaSMTP(Integer portaSMTP) {
		this.portaSMTP = portaSMTP;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getEmailFromUsuario() {
		return emailFromUsuario;
	}

	public void setEmailFromUsuario(String emailFromUsuario) {
		this.emailFromUsuario = emailFromUsuario;
	}

	public String getEmailFromSenha() {
		return emailFromSenha;
	}

	public void setEmailFromSenha(String emailFromSenha) {
		this.emailFromSenha = emailFromSenha;
	}

	public boolean isCriptografado() {
		return criptografado;
	}

	public void setCriptografado(boolean criptografado) {
		this.criptografado = criptografado;
	}
	
	public String retornarIsCriptografadoFormatado() {
		if(this.criptografado == true) {
			return "true";
		}else {
			return "false";
		}
	}
	
}
