package br.com.optimized.cadastro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.optimized.config.util.Constantes;
import br.com.optimized.generico.model.AbstractModel;

@Entity
@Table(name = "estado", schema = "gestop")
public class Estado extends AbstractModel<Long> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4489954013372443360L;

	@NotEmpty
	@Column(name = "sigla", length=2)
	private String sigla;	
	
	@NotEmpty
	@Size.List ({
	    @Size(min=3, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MIN_ESTADO_ESTADO),
	    @Size(max=60, message=Constantes.MSG_FEEDBACK_USUARIO_TAMANHO_MAX_ESTADO_ESTADO)
	})
	@Column(name = "nome")
	private String nome;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
