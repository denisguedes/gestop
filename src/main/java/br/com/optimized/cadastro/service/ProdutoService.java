package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;

import br.com.optimized.cadastro.model.Produto;
import br.com.optimized.cadastro.model.dto.ValorItensEstoque;
import br.com.optimized.cadastro.model.filter.ProdutoFilter;
import br.com.optimized.cadastro.repository.ProdutoRepository;
import br.com.optimized.generico.service.GenericBaseService;

public class ProdutoService extends GenericBaseService<ProdutoRepository, Produto, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Produto> pesquisar(ProdutoFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(ProdutoFilter filtro) {
		return repository.total(filtro);
	}
	
	public ValorItensEstoque valorItensEstoque() {
		return repository.valorItensEstoque();
	}

	public Produto findOne(Long id) {
		return repository.findById(id).orElse(new Produto());
	}
	
	public List<Produto> retornarProdutoPelaReferencia(String produto) {
		return repository.retornarProdutoPelaReferencia(produto);
	}

}
