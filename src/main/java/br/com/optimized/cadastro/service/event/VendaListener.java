package br.com.optimized.cadastro.service.event;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import br.com.optimized.cadastro.model.ItemVenda;
import br.com.optimized.cadastro.model.Produto;
import br.com.optimized.cadastro.repository.ProdutoRepository;

@Component
public class VendaListener {

	private ProdutoRepository produtoRepository;
	
	@EventListener
	public void vendaEmitida(VendaEvent vendaEvent) {
		for (ItemVenda item : vendaEvent.getVenda().getItens()) {
			Produto produto = produtoRepository.findById(item.getProduto().getId()).orElse(new Produto());
			produto.setQuantidadeEstoque(produto.getQuantidadeEstoque() - item.getQuantidade());
			produtoRepository.save(produto);
		}
	}
	
}
