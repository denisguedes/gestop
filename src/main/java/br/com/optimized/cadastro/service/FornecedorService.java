package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import br.com.optimized.cadastro.model.Fornecedor;
import br.com.optimized.cadastro.model.filter.FornecedorFilter;
import br.com.optimized.cadastro.repository.FornecedorRepository;
import br.com.optimized.generico.service.GenericBaseService;

public class FornecedorService extends GenericBaseService<FornecedorRepository, Fornecedor, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Fornecedor> pesquisar(FornecedorFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(FornecedorFilter filtro) {
		return repository.total(filtro);
	}
	
	public Optional<Fornecedor> findByCnpj(String cpfOuCnpj){
		return repository.findByCnpj(cpfOuCnpj);
	}

	public List<Fornecedor> findByNomeStartingWithIgnoreCase(String nome){
		return repository.findByNomeFantasiaStartingWithIgnoreCase(nome);
	}
	
	public List<Fornecedor> retornarFornecedorPorNomeFantasia(String nomeFantasia){
		return repository.retornarFornecedorPorNomeFantasia(nomeFantasia);
	}
	
	public Long quantidade() {
		return repository.count();
	}

}
