package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;

import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.repository.EstadoRepository;
import br.com.optimized.generico.service.GenericBaseService;;

public class EstadoService extends GenericBaseService<EstadoRepository, Estado, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Estado> listaEstados() {
		return repository.findAll();
	}
	
}
