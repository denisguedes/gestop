package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import br.com.optimized.cadastro.model.Cliente;
import br.com.optimized.cadastro.model.filter.ClienteFilter;
import br.com.optimized.cadastro.repository.ClienteRepository;
import br.com.optimized.generico.service.GenericBaseService;

public class ClienteService extends GenericBaseService<ClienteRepository, Cliente, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Cliente> pesquisar(ClienteFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(ClienteFilter filtro) {
		return repository.total(filtro);
	}
	
	public Optional<Cliente> findByCpfOuCnpj(String cpfOuCnpj){
		return repository.findByCpfOuCnpj(cpfOuCnpj);
	}

	public List<Cliente> findByNomeStartingWithIgnoreCase(String nome){
		return repository.findByNomeStartingWithIgnoreCase(nome);
	}
	
	public List<Cliente> retornarClientePorNome(String cliente){
		return repository.retornarClientePorNome(cliente);
	}
	
	public Long quantidade() {
		return repository.count();
	}

}
