package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import br.com.optimized.cadastro.model.Empresa;
import br.com.optimized.cadastro.model.filter.EmpresaFilter;
import br.com.optimized.cadastro.repository.EmpresaRepository;
import br.com.optimized.generico.service.GenericBaseService;

public class EmpresaService extends GenericBaseService<EmpresaRepository, Empresa, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Empresa> pesquisar(EmpresaFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(EmpresaFilter filtro) {
		return repository.total(filtro);
	}
	
	public Optional<Empresa> findByCnpj(String cpfOuCnpj){
		return repository.findByCnpj(cpfOuCnpj);
	}

	public List<Empresa> findByNomeStartingWithIgnoreCase(String nome){
		return repository.findByNomeFantasiaStartingWithIgnoreCase(nome);
	}
	
	public List<Empresa> retornarEmpresaPorNomeFantasia(String nomeFantasia){
		return repository.retornarEmpresaPorNomeFantasia(nomeFantasia);
	}
	
	public Long quantidade() {
		return repository.count();
	}

}
