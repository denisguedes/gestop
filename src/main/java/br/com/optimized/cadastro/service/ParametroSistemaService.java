package br.com.optimized.cadastro.service;
//package br.gov.ce.seinfra.cadastro.service;
//
//import java.io.Serializable;
//import java.util.List;
//
//import javax.faces.application.FacesMessage;
//import javax.inject.Inject;
//
//import br.gov.ce.seinfra.cadastro.model.ParametroSistema;
//import br.gov.ce.seinfra.cadastro.model.enums.ParametroSistemaEnum;
//import br.gov.ce.seinfra.cadastro.repository.ParametroSistemaRepository;
//import br.gov.ce.seinfra.config.exception.GlobalException;
//import br.gov.ce.seinfra.config.util.FacesUtil;
//import br.gov.ce.seinfra.generico.service.GenericService;
//
//public class ParametroSistemaService extends GenericService<ParametroSistema> implements Serializable {
//
//	private static final long serialVersionUID = -6607464066598605827L;
//
//	@Inject
//	private ParametroSistemaRepository parametroSistemaRepository;
//
//	public void salvar(ParametroSistemaEnum paramEnum) {
//		ParametroSistema param = buscarPorNome(paramEnum.name());
//		param.setValor(paramEnum.getValor());
//		parametroSistemaRepository.save(param);
//	}
//
//	public ParametroSistema buscarPorNome(String nome) {
//		return parametroSistemaRepository.buscarPorNome(nome);
//	}
//	
//	public void carregaCosntantesParametros() throws GlobalException {
//		List<ParametroSistema> listaParametros = findAll();
//		for (ParametroSistema parametroSistema : listaParametros) {
//				ParametroSistemaEnum paramEnum = getParametroSistemaEnum(parametroSistema.getNome());
//				paramEnum.setDescricao(parametroSistema.getDescricao());
//				paramEnum.setValor(parametroSistema.getValor());
//		}
//	}
//	
//	public ParametroSistemaEnum getParametroSistemaEnum(String nome) throws GlobalException {
//		for (ParametroSistemaEnum param : ParametroSistemaEnum.values()) {
//			if (param.name().equals(nome)) {
//				return param;
//			}
//		}
//		throw new GlobalException(FacesUtil.msgBundle.getString("msgParametroNaoEncontrado"), FacesMessage.SEVERITY_ERROR);
//	}
//
//	public String retornaTempoJobApuracao(String nome) {
//		return parametroSistemaRepository.retornaTempoJobApuracao(nome);
//	}
//	
//	public Boolean isFiltroProdutoEbsAtivo() {
//		return parametroSistemaRepository.isFiltroProdutoEbsAtivo();
//	}
//}