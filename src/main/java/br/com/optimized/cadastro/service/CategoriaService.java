package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;

import br.com.optimized.cadastro.model.Categoria;
import br.com.optimized.cadastro.model.filter.CategoriaFilter;
import br.com.optimized.cadastro.repository.CategoriaRepository;
import br.com.optimized.generico.service.GenericBaseService;

public class CategoriaService extends GenericBaseService<CategoriaRepository, Categoria, Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public List<Categoria> pesquisar(CategoriaFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(CategoriaFilter filtro) {
		return repository.total(filtro);
	}
	
	public List<Categoria> findAllByOrderByDescricao(){
		return repository.findAllByOrderByDescricao();
	}

}