package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

import br.com.optimized.cadastro.model.Venda;
import br.com.optimized.cadastro.model.dto.VendaMes;
import br.com.optimized.cadastro.model.dto.VendaOrigem;
import br.com.optimized.cadastro.model.enums.StatusVenda;
import br.com.optimized.cadastro.model.filter.VendaFilter;
import br.com.optimized.cadastro.repository.VendaRepository;
import br.com.optimized.generico.service.GenericBaseService;
import br.com.optimized.seguranca.model.Usuario;

public class VendaService extends GenericBaseService<VendaRepository, Venda, Long> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public List<Venda> pesquisar(VendaFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(VendaFilter filtro) {
		return repository.total(filtro);
	}
	
	public Venda buscarComItens(Long codigo) {
		return repository.buscarComItens(codigo);
	}
	
	public BigDecimal valorTotalNoAno() {
		return repository.valorTotalNoAno();
	}
	
	public BigDecimal valorTotalNoMes() {
		return repository.valorTotalNoMes();
	}
	
	public BigDecimal valorTicketMedioNoAno() {
		return repository.valorTicketMedioNoAno();
	}
	
	public List<VendaMes> totalPorMes(){
		return repository.totalPorMes();
	}
	
	public List<VendaOrigem> totalPorOrigem(){
		return repository.totalPorOrigem();
	}
	
	@Transactional
	public void emitir(Venda venda) {
		venda.setStatus(StatusVenda.EMITIDA);
		repository.save(venda);
	}

	@PreAuthorize("#venda.usuario == principal.usuario or hasRole('CANCELAR_VENDA')")
	@Transactional
	public void cancelar(Venda venda) {
		Venda vendaExistente = repository.findById(venda.getId()).orElse(new Venda());
		
		vendaExistente.setStatus(StatusVenda.CANCELADA);
		repository.save(vendaExistente);
	}
	
	public Map<Date, BigDecimal> valoresTotaisPorData(Integer numeroDeDias, Usuario criadoPor){
		return this.repository.valoresTotaisPorData(numeroDeDias, criadoPor);
	}
	
	public Map<Date, BigDecimal> valoresTotaisPorDataBar(Integer numeroDeDias, Usuario criadoPor){
		return this.repository.valoresTotaisPorData(numeroDeDias, criadoPor);
	}
	
}