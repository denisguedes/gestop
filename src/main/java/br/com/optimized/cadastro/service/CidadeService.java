package br.com.optimized.cadastro.service;

import java.io.Serializable;
import java.util.List;

import br.com.optimized.cadastro.model.Cidade;
import br.com.optimized.cadastro.model.Estado;
import br.com.optimized.cadastro.model.filter.CidadeFilter;
import br.com.optimized.cadastro.repository.CidadeRepository;
import br.com.optimized.generico.service.GenericBaseService;

public class CidadeService extends GenericBaseService<CidadeRepository, Cidade, Long> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public List<Cidade> pesquisar(CidadeFilter Filtro) {
		return repository.pesquisar(Filtro);
	}
	
	public Long quantidadeFiltrados(CidadeFilter filtro) {
		return repository.total(filtro);
	}
	
	public List<Cidade> listaCidadesPorCodigoEstado(Estado estado) {
		return repository.listaCidadesPorCodigoEstado(estado);
	}
	
	public Cidade listaCidadePorNome(Cidade cidade){
		return repository.listaCidadePorNome(cidade);
	}

}
