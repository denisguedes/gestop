package br.com.optimized.cadastro.service;

import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;

import br.com.optimized.cadastro.model.Email;
import br.com.optimized.cadastro.model.dto.EmailDTO;
import br.com.optimized.cadastro.repository.EmailRepository;
import br.com.optimized.config.exception.GlobalException;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.generico.service.GenericBaseService;

public class EmailService extends GenericBaseService<EmailRepository, Email, Long>{

	public Email retornarEmailSMTP() {
		return repository.retornarEmailSMTP();
	}

	@Scheduled(cron = "0 11 09 ? * MON-FRI")
	public void enviaEmail(Email emailSMTP, EmailDTO emailToTO) throws GlobalException {

		try {
			JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
			javaMailSender.setHost(emailSMTP.getServidorSMTP());
			
			javaMailSender.setPort(emailSMTP.getPortaSMTP());
			javaMailSender.setProtocol("smtp");
			javaMailSender.setUsername(emailSMTP.getEmailFromUsuario());
			javaMailSender.setPassword(emailSMTP.getEmailFromSenha());
			javaMailSender.setDefaultEncoding("utf-8");

			Properties properties = new Properties();
			properties.setProperty("username", emailSMTP.getEmailFromUsuario());
			properties.setProperty("password", emailSMTP.getEmailFromSenha());
			properties.setProperty("mail.smtp.starttls.enable", emailSMTP.retornarIsCriptografadoFormatado());
			properties.setProperty("mail.transport.protocol", "smtp");
			javaMailSender.setJavaMailProperties(properties);

			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, false);
			helper.setFrom(emailSMTP.getEmailFrom());
			helper.setSubject(emailToTO.getSubject());
			helper.setText(emailToTO.getText());
			helper.addTo(emailToTO.getEmailTo());

			javaMailSender.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackNaoFoiPossivelEnviarEmail"), FacesMessage.SEVERITY_WARN);
		}
	}
	
	public void enviaEmail(EmailDTO emailToTO) throws GlobalException {
		enviaEmail(retornarEmailSMTP(), emailToTO);
	}
	
	public void enviarEmailPadrao(String assunto, String texto, Email email) throws GlobalException {
		EmailDTO emailToTO = new EmailDTO();
		emailToTO.setSubject(assunto);
		emailToTO.setText(texto);
		emailToTO.setEmailTo(email.getEmailFrom());
		enviaEmail(email, emailToTO);
	}

}
