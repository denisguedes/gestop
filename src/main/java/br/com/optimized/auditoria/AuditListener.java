package br.com.optimized.auditoria;

import java.time.Instant;

import org.hibernate.envers.RevisionListener;

import br.com.optimized.seguranca.controller.IdentityBean;

public class AuditListener implements RevisionListener {
	
	private IdentityBean identityBean = new IdentityBean();

	@Override
	  public void newRevision(Object revisionEntity) {
	    AuditEntity revEntity = (AuditEntity) revisionEntity;
	    revEntity.setCpfUsuario(identityBean.getUsuarioLogado().getCpf()
	    		+ " - " + identityBean.getUsuarioLogado().getLogin());
	    revEntity.setDataTimestamp(Instant.now().getEpochSecond());
	  }
}
