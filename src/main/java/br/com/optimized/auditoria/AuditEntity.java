package br.com.optimized.auditoria;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

@Entity
@Table(name = "AUDITORIA_ENTIDADES", schema = "gestop")
@RevisionEntity(AuditListener.class)
public class AuditEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "rev_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_audit")
	@SequenceGenerator(name = "seq_audit", sequenceName = "SQ_AUDITORIA_ENTIDADES", schema = "gestop", allocationSize = 1)
	@RevisionNumber
	private Long id;

	@Column(name = "NUM_CPF_USUARIO")
	private String cpfUsuario;

	@Column(name = "DAT_TIMESTAMP")
	@RevisionTimestamp
	private Long dataTimestamp;
	
    @ElementCollection
    @JoinTable(name = "REVCHANGES", joinColumns = @JoinColumn(name = "REV"))
    @Column(name = "ENTITYNAME")
    @ModifiedEntityNames
	private Set<String> modifiedEntitiesNames;
	
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpfUsuario() {
		return cpfUsuario;
	}

	public void setCpfUsuario(String cpfUsuario) {
		this.cpfUsuario = cpfUsuario;
	}

	public Long getDataTimestamp() {
		return dataTimestamp;
	}

	public void setDataTimestamp(Long dataTimestamp) {
		this.dataTimestamp = dataTimestamp;
	}
}