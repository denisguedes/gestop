package br.com.optimized.config.security;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.optimized.cadastro.model.enums.ParametroSistemaEnum;
import br.com.optimized.config.exception.GlobalException;
import br.com.optimized.config.util.CDIServiceLocator;
import br.com.optimized.config.util.Constantes;
import br.com.optimized.config.util.FacesUtil;
import br.com.optimized.seguranca.model.PerfilPermissao;
import br.com.optimized.seguranca.model.Usuario;
import br.com.optimized.seguranca.repository.UsuarioRepository;

/**
 * Service dedicado para fazer o meio de campo entre JSF e o Spring Security
 */
@Service
public class AppUserDetailsService implements UserDetailsService {

	private UsuarioRepository usuarioRepository = CDIServiceLocator.getBean(UsuarioRepository.class);

	private HttpServletRequest request = CDIServiceLocator.getBean(HttpServletRequest.class);
	private FacesUtil facesUtils = new FacesUtil();
	private Logger logger = Logger.getLogger(this.getClass());
	public static final int ZERA_TENTATIVAS = -1;
	public static final int ZERO = 0;
	public AppUserDetailsService() {
		super();
	}

	@Override
	public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
		try {
			Usuario usuario = usuarioRepository.findByLogin(login);
			verificaSeExisteUsuario(usuario);
			verificaSeOUsuarioEstaInativo(usuario);

			if (!FacesUtil.compararSenha(request.getParameter("senha"), usuario.getSenha())) {
				verificaNumDeTentativasEAtualiza(usuario);
				throw new UsernameNotFoundException(
						FacesUtil.msgBundle.getString("msgFeedbackUsuarioOuSenhaInvalidos"));
			} 
			else {
				return new UsuarioSistema(usuario, obtemGruposdoUsuario(usuario));
			}
		} catch (GlobalException e) {
			enviaMensagemDeFeedBackParaUsuario(e.getMsg());
			throw new UsernameNotFoundException(e.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			facesUtils.msgFeedback(null, "Falha ao executar ação",
					FacesMessage.SEVERITY_ERROR);
			logger.error("Falha ao executar ação", e);
			throw new UsernameNotFoundException(e.getMessage());
		}
	}

	private void verificaSeOUsuarioEstaInativo(Usuario usuario) throws GlobalException {
		if (usuario.getIdentificador().equalsIgnoreCase(Constantes.CAMPO_STATUS_INATIVO)) {
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioInativo"),
					FacesMessage.SEVERITY_ERROR);
		}

	}

	private Collection<? extends GrantedAuthority> obtemGruposdoUsuario(Usuario usuario) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		List<PerfilPermissao> permissoesPerfil = usuario.getPerfil().getPermissoes();
		permissoesPerfil.forEach(p -> authorities.add(new SimpleGrantedAuthority("ROLE_" + p.getPermissao().getDescricao().toUpperCase())));

		return authorities;
	}

	private void verificaSeExisteUsuario(Usuario usuario) throws GlobalException {
		if (usuario == null) {
			throw new GlobalException(FacesUtil.msgBundle.getString("msgFeedbackUsuarioOuSenhaInvalidos"),
					FacesMessage.SEVERITY_ERROR);
		}
	}

	private void verificaNumDeTentativasEAtualiza(Usuario usuario) throws GlobalException {
		if (usuario.getNumeroTentativasLogin() == ParametroSistemaEnum.TENTATIVAS_ACESSO.intValue()) {
			if (verificaQtdeMinutosDoUltimoLoginAteDataAtual(usuario) >= ParametroSistemaEnum.TEMPO_BLOQUEIO
					.intValue()) {
				usuario.setNumeroTentativasLogin(ZERO);
				atualizaNumeroTentativasErradasUsuario(usuario);
				enviaMensagemDeFeedBackParaUsuario(msgUsuarioBloqueadoAposVariasTentativasDeLogin());
			} else {
				enviaMensagemDeFeedBackParaUsuario(msgUsuarioBloqueadoTemporariamente());
			}
		} else {
			atualizaNumeroTentativasErradasUsuario(usuario);
			if (usuario.getNumeroTentativasLogin() == ParametroSistemaEnum.TENTATIVAS_ACESSO.intValue()) {
				enviaMensagemDeFeedBackParaUsuario(msgUsuarioBloqueadoTemporariamente());
			} else {
				enviaMensagemDeFeedBackParaUsuario(msgUsuarioBloqueadoAposVariasTentativasDeLogin());
			}
		}
	}

	private long verificaQtdeMinutosDoUltimoLoginAteDataAtual(Usuario usuario) {
		Calendar dataAtual = Calendar.getInstance();
		dataAtual.setTime(new Date());

		Calendar dataUltimaTentativaLogin = Calendar.getInstance();
		dataUltimaTentativaLogin.setTime(usuario.getUltimaTentativaLogin());

		long qtdeMinutos = ((dataAtual.getTimeInMillis() - dataUltimaTentativaLogin.getTimeInMillis()) / 60000);
		return qtdeMinutos;
	}

	public Usuario atualizaNumeroTentativasErradasUsuario(Usuario usuario) throws GlobalException {
		usuario.setNumeroTentativasLogin(usuario.getNumeroTentativasLogin() + 1);
		usuario.setUltimaTentativaLogin(new Date());

		return usuarioRepository.save(usuario);
	}

	private String msgUsuarioBloqueadoAposVariasTentativasDeLogin() {
		return FacesUtil.msgBundle.getString("msgFeedbackUsuarioOuSenhaInvalidos") + " "
				+ FacesUtil.getString("msgFeedbackAposCincoTentativasUsuarioBloqueado",
						ParametroSistemaEnum.TENTATIVAS_ACESSO.intValue(),
						ParametroSistemaEnum.TEMPO_BLOQUEIO.intValue());
	}

	private String msgUsuarioBloqueadoTemporariamente() {
		return FacesUtil.getString("msgFeedbackUsuarioBloqueadoPor", ParametroSistemaEnum.TEMPO_BLOQUEIO.intValue());
	}

	public void enviaMensagemDeFeedBackParaUsuario(String msg) {
		if (request.getSession().getAttribute("msg") != null) {
			request.getSession().removeAttribute("msg");
		}
		request.getSession().setAttribute("msg", msg);
	}
}
