
package br.com.optimized.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	private static UserDetailsService usuarioService = new AppUserDetailsService();
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	   auth.userDetailsService(usuarioService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	//Controle feito pelo JSF
    	http.csrf().disable();
    	    	
    	//Pagina de acesso negado
        http.exceptionHandling().accessDeniedPage("/access.xhtml");
        
        //Libera todos os recursos do JSF
        http.authorizeRequests().antMatchers("/javax.faces.resource/**").permitAll();
        
        //CONTROLA O ACESSO A PAGINA PROTEGIDA.   
        
      //Categoria
//        http.authorizeRequests().antMatchers("/categoria/categoriaList.xhtml").hasAnyRole("CATEGORIA", "CATEGORIA.GERENCIAR", "CATEGORIA.GERENCIAR.PESQUISAR");
//        http.authorizeRequests().antMatchers("/categoria/categoriaForm.xhtml").hasAnyRole("CATEGORIA", "CATEGORIA.GERENCIAR", "CATEGORIA.GERENCIAR.PESQUISAR");
    	
        //Usuário
        http.authorizeRequests().antMatchers("/usuario/usuarioList.xhtml").hasAnyRole("USUARIO", "USUARIO.GERENCIAR" , "USUARIO.GERENCIAR.PESQUISAR");
        http.authorizeRequests().antMatchers("/usuario/usuarioForm.xhtml").hasAnyRole("USUARIO.GERENCIAR.INSERIR" , "USUARIO.GERENCIAR.EDITAR" , "USUARIO.GERENCIAR.VISUALIZAR");
        
        //Perfil
        http.authorizeRequests().antMatchers("/perfil/perfilList.xhtml").hasAnyRole("PERFIL", "PERFIL.GERENCIAR", "PERFIL.GERENCIAR.PESQUISAR");
        http.authorizeRequests().antMatchers("/perfil/perfilForm.xhtml").hasAnyRole("PERFIL", "PERFIL.GERENCIAR", "PERFIL.GERENCIAR.PESQUISAR");
        
        //Permissao
        http.authorizeRequests().antMatchers("/permissao/permissaoList.xhtml").hasAnyRole("PERMISSAO", "PERMISSAO.GERENCIAR", "PERMISSAO.GERENCIAR.PESQUISAR");
        http.authorizeRequests().antMatchers("/permissao/permissaoForm.xhtml").hasAnyRole("PERMISSAO", "PERMISSAO.GERENCIAR", "PERMISSAO.GERENCIAR.PESQUISAR");

        //Email SMTP
        http.authorizeRequests().antMatchers("/form-cad/cad-email-smtp.xhtml").hasAnyRole("EMAILSMTP", "EMAILSMTP.GERENCIAR", "EMAILSMTP.GERENCIAR.PESQUISAR");
        
        //Seguranca
        http.authorizeRequests().antMatchers("/parametroSistema/formSeguranca.xhtml").hasAnyRole("SEGURANCA", "SEGURANCA.GERENCIAR", "SEGURANCA.GERENCIAR.PESQUISAR");
        
        //Login
    	http.formLogin().loginPage("/login.xhtml").permitAll()
		.defaultSuccessUrl("/dashboard.xhtml", true)
		.failureUrl("/login.xhtml")
		.usernameParameter("usuario")
		.passwordParameter("senha");
    	
    	//Logout
        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.xhtml");

        // Todas as requisicoes para partes internas da aplicacao devem ser autenticadas
		http.authorizeRequests().anyRequest().authenticated();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/resources/**");
        web.ignoring().antMatchers("/javax.faces.resource/**");
    }
}

