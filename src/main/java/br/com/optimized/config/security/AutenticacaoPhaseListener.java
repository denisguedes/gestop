package br.com.optimized.config.security;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

/**
 * @author Denis Guedes
 * @since 22.02.2017
 */
public class AutenticacaoPhaseListener implements PhaseListener {

	private static final long serialVersionUID = -3155148099818905433L;

	@Override
	public void afterPhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
    public void beforePhase(PhaseEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        if (session != null) {
            String mensagem = (String) session.getAttribute("msg");
 
            if (mensagem != null) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, null));
                session.setAttribute("msg", null);
            }
        }
    }
 
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

}