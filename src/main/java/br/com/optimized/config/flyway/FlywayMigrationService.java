package br.com.optimized.config.flyway;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.Extension;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The MigrationService calls the DB Migrations during startup of the Application
 */
@ApplicationScoped
public class FlywayMigrationService implements Extension {

    private static final Logger LOG = LoggerFactory.getLogger(FlywayMigrationService.class);

    @PostConstruct
    public void onStartup() {
        LOG.info("Setting Up Flyway ");
        Flyway flyway = Flyway.configure().dataSource("jdbc:postgresql://localhost:5432/dbgestop", "postgres", "postgres").load();
        flyway.migrate();
        LOG.error("Database migration complete");
    }

}
