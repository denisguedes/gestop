package br.com.optimized.config.exception;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage.Severity;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class GlobalException extends Exception {

	private static final long serialVersionUID = 6246165264282823061L;

	private Logger logger;
	private String msg;
	private List<String> msgs;
	private Severity tipoMensagem;
	private HashMap<String, Object[]> msgsComParametro;
	private Object[] parameters;

	public GlobalException(String msg, Severity tipoMensagem, final Object[] parameters) {
		super();
		this.msg = msg;
		this.tipoMensagem = tipoMensagem;
		this.parameters = parameters;
		log();
	}

	public GlobalException(String msg, Severity tipoMensagem) {
		super();
		this.msg = msg;
		this.tipoMensagem = tipoMensagem;
		log();
	}

	public GlobalException(List<String> msgs, Severity tipoMensagem) {
		super();
		this.msgs = msgs;
		this.tipoMensagem = tipoMensagem;
		log();
	}

	public GlobalException(HashMap<String, Object[]> msgsComParametro, Severity tipoMensagem) {
		super();
		this.tipoMensagem = tipoMensagem;
		this.msgsComParametro = msgsComParametro;
		log();
	}

	private void log() {
		Class<?> clazz = null;
		int lineNumber = 0;
		String fileName = "";
		try {
			clazz = Class.forName(this.getStackTrace()[0].getClassName());
			fileName = this.getStackTrace()[0].getFileName();
			lineNumber = this.getStackTrace()[0].getLineNumber();
		} catch (ClassNotFoundException e) {
			clazz = this.getClass();
		} finally {
			logger = Logger.getLogger(clazz);
		}
		if (msgs == null || msgs.isEmpty()) {
			logger.log(fileName + ":" + lineNumber, Level.ERROR, msg, this);
		} else {
			logger.log(fileName + ":" + lineNumber, Level.ERROR, msgs, this);
		}
	}

	/*
	 * GETS E SETS
	 */
	public String getMsg() {
		if (parameters != null) {
			msg = MessageFormat.format(msg, parameters);
		}
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<String> getMsgs() {
		if (msgs == null) {
			msgs = new ArrayList<String>();
		}

		if (msgs.isEmpty()) {
			if (getMsg() != null && !getMsg().isEmpty())
				msgs.add(getMsg());
			if (msgsComParametro != null && !msgsComParametro.isEmpty()) {
				for (String msgParam : msgsComParametro.keySet()) {
					msgs.add(msgParam);
				}
			}
		}
		return msgs;
	}

	public void setMsgs(List<String> msgs) {
		this.msgs = msgs;
	}

	public Severity getTipoMensagem() {
		return tipoMensagem;
	}

	public void setTipoMensagem(Severity tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

	public HashMap<String, Object[]> getMsgsComParametro() {
		return msgsComParametro;
	}

	public void setMsgsComParametro(HashMap<String, Object[]> msgsComParametro) {
		this.msgsComParametro = msgsComParametro;
	}

	public Object[] getParameters() {
		return parameters;
	}

	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}

}
