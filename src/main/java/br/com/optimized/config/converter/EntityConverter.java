package br.com.optimized.config.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.optimized.generico.model.AbstractModel;

@SuppressWarnings("rawtypes")
@FacesConverter(value="entityConverter", forClass = AbstractModel.class)
public class EntityConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value) {
        if (value != null && !value.isEmpty()) {
            return (AbstractModel) uiComponent.getAttributes().get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value instanceof AbstractModel) {
            AbstractModel entity= (AbstractModel) value;
            if (entity != null && entity instanceof AbstractModel && entity.getId() != null) {
                uiComponent.getAttributes().put( entity.getId().toString(), entity);
                return entity.getId().toString();
            }
        }
        return "";
    }
}