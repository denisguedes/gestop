package br.com.optimized.config.converter;

import java.util.List;
import java.util.Objects;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

@SuppressWarnings("rawtypes")
@FacesConverter(value = "primeFacesPickListConverter")
public class PrimeFacesPickListConverter implements Converter {

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object entity) {
		if (entity == null)
			return "";
		return String.valueOf(entity.hashCode());
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String uuid) {
		Object ret = null;
		if (uuid == null || uuid.equals(""))
			return null;

		if (component instanceof PickList) {
			final Object dualList = ((PickList) component).getValue();
			final DualListModel dl = (DualListModel) dualList;
			ret = retrieveObject(dl.getSource(), uuid);
			if (ret == null)
				ret = retrieveObject(dl.getTarget(), uuid);
		}

		return ret;
	}

	/**
	 * Function retrieves the object by its hashCode. Because this has to be
	 * generic, typing is raw - and therefore disable IDE warning.
	 *
	 * @param objects
	 *            list of arbitrary objects
	 * @param uuid
	 *            hashCode of the object to retrieve
	 * @return correct object with corresponding hashCode or null, if none found
	 */
	@SuppressWarnings("unchecked")
	private Object retrieveObject(final List objects, final String uuid) {
		return objects.stream().filter(Objects::nonNull).filter(obj -> uuid.equals(String.valueOf(obj.hashCode())))
				.findFirst().orElse(null);
	}

}