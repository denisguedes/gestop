package br.com.optimized.config.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import br.com.optimized.seguranca.model.Permissao;

@FacesConverter(value = "perfilPermissaoConverter")
public class PerfilPermissaoConverter implements Converter<Object> {
	
  @Override
  @SuppressWarnings("unchecked")
  public Object getAsObject(FacesContext fc, UIComponent comp, String value) {
	DualListModel<Permissao> model = (DualListModel<Permissao>) ((PickList) comp).getValue();
      for (Permissao perfilPermissao : model.getSource()) {
          if (perfilPermissao.getId().equals(Long.valueOf(value))) {
              return perfilPermissao;
          }
      }
      for (Permissao perfilPermissao : model.getTarget()) {
          if (perfilPermissao.getId().equals(Long.valueOf(value))) {
              return perfilPermissao;
          }
      }
      return null;
  }

  @Override
  public String getAsString(FacesContext fc, UIComponent comp, Object value) {
      return ((Permissao) value).getId().toString();
  }
}
