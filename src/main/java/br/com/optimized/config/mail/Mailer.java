package br.com.optimized.config.mail;

import java.io.Serializable;
import java.io.StringWriter;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import br.com.optimized.cadastro.model.Venda;
import br.com.optimized.config.util.FacesUtil;

@Service
public class Mailer implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7737118357993360880L;
	
	private FacesUtil facesUtils = new FacesUtil();
	
	private static VelocityEngine VELOCITY_ENGINE;

	private static Logger logger = LoggerFactory.getLogger(Mailer.class);
	
	static {
		VELOCITY_ENGINE = new VelocityEngine();
		VELOCITY_ENGINE.setProperty(RuntimeConstants.RESOURCE_LOADER, "class");
		VELOCITY_ENGINE.setProperty("class.resource.loader.class", ClasspathResourceLoader.class.getName());
		VELOCITY_ENGINE.setProperty("runtime.log.logsystem.log4j.category", "velocity");
		VELOCITY_ENGINE.setProperty("runtime.log.logsystem.log4j.logger", "velocity");

		VELOCITY_ENGINE.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS,
				"org.apache.velocity.runtime.log.Log4JLogChute");

		VELOCITY_ENGINE.setProperty("runtime.log.logsystem.class",
				"org.apache.velocity.runtime.log.SimpleLog4JLogSystem");

		VELOCITY_ENGINE.init();
	}
	
	private StringBuffer getEmail(Venda venda) {
		VelocityContext context = new VelocityContext();
		Template template = new Template();
		template.setEncoding("UTF-8");

		template = VELOCITY_ENGINE.getTemplate("pedido.vm");
		context.put("venda", venda);

		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		return writer.getBuffer();
	}

	public void enviar(Venda venda) {
		try {
			JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
			javaMailSender.setHost("smtp.gmail.com");
			javaMailSender.setPort(587);
			javaMailSender.setProtocol("smtp");
			javaMailSender.setUsername("denisguedes23@gmail.com");
			javaMailSender.setPassword("Oliveira.091");
			javaMailSender.setDefaultEncoding("utf-8");

			Properties properties = new Properties();
			properties.setProperty("username", "denisguedes23@gmail.com");
			properties.setProperty("password", "Oliveira.091");
			properties.setProperty("mail.smtp.starttls.enable", "true");
			properties.setProperty("mail.transport.protocol", "smtp");
			javaMailSender.setJavaMailProperties(properties);
			
			MimeMessage msg = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, false);
			helper.setFrom("denisguedes23@gmail.com");
			helper.setSubject(String.format("Oh Coisa Linda - Venda nº %d", venda.getId()));
			helper.setText(getEmail(venda).toString(), true);
			helper.addTo(venda.getCliente().getEmail());
	
			javaMailSender.send(msg);
	
		} catch (MessagingException e) {
			facesUtils.msgFeedback(null, "Falha ao enviar email",	FacesMessage.SEVERITY_ERROR);
			logger.error("Erro enviando e-mail", e);
		}
	}
	
}
