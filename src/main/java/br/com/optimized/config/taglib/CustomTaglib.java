package br.com.optimized.config.taglib;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class CustomTaglib {
	
	private CustomTaglib() {}
	
	public static String datePattern() {
		return "MM/yyyy";
	}
	
	public static String customFormatDate(Date date) {
		if(date != null) {
			DateFormat format = new SimpleDateFormat(datePattern());
			return format.format(date);
		}
		return "";
	}

}
