package br.com.optimized.config.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.MaskFormatter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.primefaces.event.FileUploadEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.optimized.config.exception.GlobalException;

public class FacesUtil {
	
	public static ResourceBundle msgBundle;
	private static final Pattern pattern = Pattern.compile(Constantes.EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
	
	static {
		msgBundle = ResourceBundle.getBundle("br.com.optimized.Messages", new Locale("pt", "BR"));
    }
	
	public static String getString(String key, Object... props) {
		try {
			if (props != null && props.length > 0) {
				return MessageFormat.format(msgBundle.getString(key), props);
			}
			return msgBundle.getString("UTF-8");
		} catch (MissingResourceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public void msgFeedback(String tagIdent, String mensagem, Severity severity) {

		FacesMessage facesMessage = new FacesMessage(severity, mensagem, mensagem);

		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(tagIdent, facesMessage);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);	
	}
	
	public void msgFeedback(String tagIdent, String mensagem, Severity severity, final Object[] parameters) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
        String formattedMessage = MessageFormat.format(mensagem, parameters);
        FacesMessage facesMessage = new FacesMessage(severity, formattedMessage, formattedMessage);
		facesContext.addMessage(tagIdent, facesMessage);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);	
	}
	
	public void msgFeedback(String tagIdent, HashMap<String,  Object[]> msgsComParametro, Severity severity) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		for (Entry<String, Object[]> msgParameter : msgsComParametro.entrySet()) {
		    String formattedMessage = MessageFormat.format(msgParameter.getKey(), msgParameter.getValue());
		    FacesMessage facesMessage = new FacesMessage(severity, formattedMessage, formattedMessage);
		    facesContext.addMessage(tagIdent, facesMessage);
		}
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);	
	}
	
	public void msgFeedback(String tagIdent, List<String> msgs, FacesMessage.Severity facesMessage) {
		if (msgs != null && msgs.size() > 0) {
			for (String msgAux : msgs) {
				msgFeedback(null, msgAux, facesMessage);
			}
		}	
	}
	
	//METODO OBSOLETO A SER DELETADO, AINDA USADO
	public void msgSucesso(String tagIdent , String mensagem) {

		FacesMessage facesMessage = new FacesMessage(mensagem);

		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(tagIdent, facesMessage);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);	
	}
	
	//METODO OBSOLETO A SER DELETADO, AINDA USADO
	public void msgAtencao(String tagIdent , String mensagem) {

        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, mensagem, mensagem);

        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(tagIdent, facesMessage);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);	
    }
	
	//METODO OBSOLETO A SER DELETADO, AINDA USADO
	public void msgErro(String tagIdent , String mensagem) {

        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, mensagem, mensagem);

        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(tagIdent, facesMessage);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);	
    }
	
	 // Getters -----------------------------------------------------------------------------------

    public static Object getSessionMapValue(String key) {
    	return FacesContext.getCurrentInstance().getViewRoot().getViewMap().get(key);
    }

    // Setters -----------------------------------------------------------------------------------

    public static void setSessionMapValue(String key, Object value) {
    	FacesContext.getCurrentInstance().getViewRoot().getViewMap().put(key, value);
    }
    
    /*
     * Métodos de processamento genérico
     */
    
    // Validação de campo somente números
 	public boolean validarSomenteNumeros(String dadosEntrada) {
 		dadosEntrada = dadosEntrada.trim();
 		char[] c = dadosEntrada.toCharArray();
 		boolean retorno = true;
 		for (int i = 0; i < c.length; i++) {
 			if (!Character.isDigit(c[i])) {
 				retorno = false;
 				break;
 			}
 		}
 		return retorno;
 	}

 	// Validação de campo de data
 	public boolean validarData(String dataEntrada) {
 		dataEntrada = dataEntrada.trim();

 		// Validar do tamanho do campo
 		if (dataEntrada.length() != 10) {
 			return false;
 		}

 		// Validar integridade do campo de data
 		String[] partesData = dataEntrada.split("/");
 		if (partesData.length == 3) {
 			if (partesData[0].length() == 2 && partesData[1].length() == 2 && partesData[2].length() == 4) {
 				if (validarSomenteNumeros(partesData[0]) && validarSomenteNumeros(partesData[1])
 						&& validarSomenteNumeros(partesData[2])) {
 					int dia = Integer.parseInt(partesData[0]);
 					int mes = Integer.parseInt(partesData[1]);
 					int ano = Integer.parseInt(partesData[2]);
 					if ((dia >= 1 && dia <= 31) && (mes >= 1 && mes <= 12) && (ano > 1900)) {
 						return true;
 					}
 				}

 			}
 		}
 		return false;
 	}

 	// Retirada de acentos e caracteres especiais de String
 	public String toUpperCaseSansAccent(String txt) {

 		final String UPPERCASE_ASCII = "AEIOU" // grave
 				+ "AEIOUY" // acute
 				+ "AEIOUY" // circumflex
 				+ "AON" // tilde
 				+ "AEIOUY" // umlaut
 				+ "A" // ring
 				+ "C" // cedilla
 				+ "OU" // double acute
 		;

 		final String UPPERCASE_UNICODE = "\u00C0\u00C8\u00CC\u00D2\u00D9" + "\u00C1\u00C9\u00CD\u00D3\u00DA\u00DD"
 				+ "\u00C2\u00CA\u00CE\u00D4\u00DB\u0176" + "\u00C3\u00D5\u00D1" + "\u00C4\u00CB\u00CF\u00D6\u00DC\u0178"
 				+ "\u00C5" + "\u00C7" + "\u0150\u0170";

 		if (txt == null) {
 			return null;
 		}
 		String txtUpper = txt.toUpperCase();
 		StringBuilder sb = new StringBuilder();
 		int n = txtUpper.length();
 		for (int i = 0; i < n; i++) {
 			char c = txtUpper.charAt(i);
 			int pos = UPPERCASE_UNICODE.indexOf(c);
 			if (pos > -1) {
 				sb.append(UPPERCASE_ASCII.charAt(pos));
 			} else {
 				sb.append(c);
 			}
 		}
 		return sb.toString();
 	}
 	
 	public boolean validarDataInicialMenorDataFinal(Date dataInicial, Date dataFinal){
 		try{
	 		if(dataInicial.after(dataFinal)){
	 			return false;
	 		}else{
				return true;
			}
 		}catch(Exception e){
 			
 			return false;
 		}
 	}
 	
 	public static boolean validarEmail(String email){
	    Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	}
 	
 	public boolean temInterseccaoPeriodoDouble(Double valorInicial1, Double valorFinal1, Double valorInicial2, Double valorFinal2) {
		if((valorInicial1 < valorInicial2 && valorInicial1 < valorFinal2) && (valorFinal1 < valorInicial2 && valorFinal1 < valorFinal2)) {
			return false;
		}
		return true;
	}
	
	public boolean existeInterseccaoPeriodoDouble(Double valorInicial1, Double valorFinal1, Double valorInicial2, Double valorFinal2) {
		if(valorInicial1 == null || valorFinal1 == null || valorInicial2 == null || valorFinal2 == null){
			return false;
		}
		if(!temInterseccaoPeriodoDouble(valorInicial1, valorFinal1, valorInicial2, valorFinal2)) {
			return false;
		}
		if(!temInterseccaoPeriodoDouble(valorInicial2, valorFinal2, valorInicial1, valorFinal1)) {
			return false;
		}
		return true;
	}
 	
 	public boolean temInterseccaoData(Date dataInicial1, Date dataFinal1, Date dataInicial2, Date dataFinal2) {
		if((dataInicial1.before(dataInicial2) && dataInicial1.before(dataFinal2)) && (dataFinal1.before(dataInicial2) && dataFinal1.before(dataFinal2))) {
			return false;
		}
		return true;
	}
	
	public boolean existeInterseccaoData(Date dataInicial1, Date dataFinal1, Date dataInicial2, Date dataFinal2) {
		if(dataInicial1 == null || dataFinal1 == null || dataInicial2 == null || dataFinal2 == null){
			return false;
		}
		if(!temInterseccaoData(dataInicial1, dataFinal1, dataInicial2, dataFinal2)) {
			return false;
		}
		if(!temInterseccaoData(dataInicial2, dataFinal2, dataInicial1, dataFinal1)) {
			return false;
		}
		return true;
	}
	
	public boolean validaDataNoPeriodo(Date dataInicial, Date dataFinal, Date dataBusca) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		if(dataInicial ==  null || dataFinal == null || dataBusca ==  null){
			return false;
		}
		if ((dataBusca.after(dataInicial) && dataBusca.before(dataFinal)) 
				|| df.format(dataBusca).equals(df.format(dataInicial))
				|| df.format(dataBusca).equals(df.format(dataFinal))) {
			return true;
		}
       
		return false;
	}
	
	public Date montaData(int ano, int mes, int dia) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, ano); 
		c.set(Calendar.MONTH, mes); 
		c.set(Calendar.DAY_OF_MONTH, dia);
		
		return c.getTime();
	}
	
	public Date montaDataInicialMes(int mes , int ano){
		if(mes == 0){
			return montaData(ano, 0, 1);
		}else if(mes == 1){
			return montaData(ano, 1, 1);
		}else if(mes == 2){
			return montaData(ano, 2, 1);
		}else if(mes == 3){
			return montaData(ano, 3, 1);
		}else if(mes == 4){
			return montaData(ano, 4, 1);
		}else if(mes == 5){
			return montaData(ano, 5, 1);
		}else if(mes == 6){
			return montaData(ano, 6, 1);
		}else if(mes == 7){
			return montaData(ano, 7, 1);
		}else if(mes == 8){
			return montaData(ano, 8, 1);
		}else if(mes == 9){
			return montaData(ano, 9, 1);
		}else if(mes == 10){
			return montaData(ano, 10, 1);
		}else if(mes == 11){
			return montaData(ano, 11, 1);
		}	
		return null;
	}
	
	public Date montaDataFinalMes(int mes , int ano){
		if(mes == 0){
			return montaData(ano, 0, 31);
		}else if(mes == 1){
			return montaData(ano, 1, 28);
		}else if(mes == 2){
			return montaData(ano, 2, 31);
		}else if(mes == 3){
			return montaData(ano, 3, 30);
		}else if(mes == 4){
			return montaData(ano, 4, 31);
		}else if(mes == 5){
			return montaData(ano, 5, 30);
		}else if(mes == 6){
			return montaData(ano, 6, 31);
		}else if(mes == 7){
			return montaData(ano, 7, 31);
		}else if(mes == 8){
			return montaData(ano, 8, 30);
		}else if(mes == 9){
			return montaData(ano, 9, 31);
		}else if(mes == 10){
			return montaData(ano, 10, 30);
		}else if(mes == 11){
			return montaData(ano, 11, 31);
		}
		return null;
	}
	
	public Double retornaDoubleFormatadoCasaDecimal(Double valor , int quantCasasDecimais)throws NumberFormatException{
		DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(quantCasasDecimais);
		return Double.parseDouble(df.format(valor).replaceAll(",","."));
	}
	
	public int retornaAnoDeUmaData(Date data) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(data);
		
		return dataCalendar.get(Calendar.YEAR);
	}
	
	public int retornaMesDeUmaData(Date data) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(data);
		
		return dataCalendar.get(Calendar.MONTH) + 1;
	}
	
	public int retornaDiaDeUmaData(Date data) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(data);
		
		return dataCalendar.get(Calendar.DAY_OF_MONTH);
	}
	
	public boolean validarTamanhoNomeArquivo(FileUploadEvent event , int tamanhoMaxPermitido) throws FileNotFoundException, IOException {
		if(event.getFile().getFileName().length() <= tamanhoMaxPermitido){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean compararSenha(String senhaFornecida, String senhaUsuarioBd) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.matches(senhaFornecida, senhaUsuarioBd);
    }
	
	public static String criptografarSenha(String senha) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(senha);
	}
	
	public static boolean senhaForte(String senha) {
	    boolean achouNumero = false;
	    boolean achouMaiuscula = false;
	    boolean achouMinuscula = false;
	    boolean achouSimbolo = false;
	    
	    for (char c : senha.toCharArray()) {
	         if (c >= '0' && c <= '9') {
	             achouNumero = true;
	         } else if (c >= 'A' && c <= 'Z') {
	             achouMaiuscula = true;
	         } else if (c >= 'a' && c <= 'z') {
	             achouMinuscula = true;
	         } else {
	             achouSimbolo = true;
	         }
	    }
	    
	    return achouNumero && achouMaiuscula && achouMinuscula && achouSimbolo;
	}
	
	public static String converteTimestampToDataHora(Long timestamp) {
		LocalDateTime triggerTime =
			       LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp),
			                               TimeZone.getDefault().toZoneId());
		return triggerTime.toString();
	}
	
	public static String retornarMes(Integer indiceMes){
		String mes = null;
		if (indiceMes == 1) {
			mes = Constantes.JANEIRO;
		} else if (indiceMes == 2) {
			mes = Constantes.FEVEREIRO;
		} else if (indiceMes == 3) {
			mes = Constantes.MARCO;
		} else if (indiceMes == 4) {
			mes = Constantes.ABRIL;
		} else if (indiceMes == 5) {
			mes = Constantes.MAIO;
		} else if (indiceMes == 6) {
			mes = Constantes.JUNHO;
		} else if (indiceMes == 7) {
			mes = Constantes.JULHO;
		} else if (indiceMes == 8) {
			mes = Constantes.AGOSTO;
		} else if (indiceMes == 9) {
			mes = Constantes.SETEMBRO;
		} else if (indiceMes == 10) {
			mes = Constantes.OUTUBRO;
		} else if (indiceMes == 11) {
			mes = Constantes.NOVEMBRO;
		} else if (indiceMes == 12) {
			mes = Constantes.DEZEMBRO;
		}
		return mes;
	}
	
	public static Date formataStringParaDate(String data) throws Exception { 
		if (data == null || data.equals(""))
			return null;
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            date = (java.util.Date)formatter.parse(data);
        } catch (ParseException e) {            
            throw e;
        }
        return date;
	}
	
	public static XMLGregorianCalendar coverterDateParaXmlGregorianCalendar(Date data) throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(data);
		XMLGregorianCalendar dataConvertida = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		
		return dataConvertida;
	}
	
	public static Date coverterXmlGregorianCalendarParaDate(XMLGregorianCalendar data) throws DatatypeConfigurationException {
		GregorianCalendar c = new GregorianCalendar();
		c.set(Calendar.YEAR, data.getYear()); 
		c.set(Calendar.MONTH, data.getMonth()); 
		c.set(Calendar.DAY_OF_MONTH, data.getDay()); 

		return c.getTime();
	}
	
	public static String removeMascaraCNPJ(String cnpjComMascara) {
		return cnpjComMascara.replaceAll("[./-]", "");
	}
	
	public static String formataCNPJ(String cnpjSemFormatacao) throws ParseException {
		MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
		mask.setValueContainsLiteralCharacters(false);
		return mask.valueToString(cnpjSemFormatacao);
	}
	
	public static Date converteStringParaDate(String data) throws Exception { 
		if (data == null || data.equals(""))
			return null;
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
            date = (java.util.Date)formatter.parse(data);
        } catch (ParseException e) {            
            throw e;
        }
        return date;
	}

	public static String validaString(Object value) {
		if (value != null)
			return value.toString();
		else
			return null;
	}
	
	public static Integer validaInteger(Object value) {
		if (value != null)
			return Integer.parseInt(value.toString());
		else
			return null;
	}

	public static Double validaDouble(Object value) {
		if (value != null)
			return Double.parseDouble(value.toString());
		else
			return null;
	}
	
	public static Long validaLong(Object value) {
		if (value != null)
			return Long.parseLong(value.toString());
		else
			return null;
	}
	
	public static BigDecimal validaBigDecimal(Object value) {
		if (value != null)
			return new BigDecimal(value.toString());
		else
			return null;
	}
	
	public static Date validaDate(Object value) { 
		if (value != null)
			return new Date(((Timestamp) value).getTime());
		else
			return null;
	}
	
	public static Boolean validaBoolean(Object value) {
		if (value != null) 
			return value.toString().equals("0") ? false : value.toString().equals("1") ? true : null;
		else
			return null;
	}
	
	public void validaTamanhoDoNomeDoArquivo(FileUploadEvent event , int tamanhoMaxPermitido) throws FileNotFoundException, IOException, GlobalException {
		if(event.getFile().getFileName().length() > tamanhoMaxPermitido)
			throw new GlobalException(FacesUtil.msgBundle.getString("msgTamanhoNomeArquivoInvalido"), FacesMessage.SEVERITY_WARN);
	}
	
	public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(Date data) {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(data);
		XMLGregorianCalendar xMLGregorianCalendar = null;
		try {
			xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			xMLGregorianCalendar.setYear(gregorianCalendar.get(GregorianCalendar.YEAR));
			xMLGregorianCalendar.setMonth(gregorianCalendar.get(GregorianCalendar.MONTH)+1);
			xMLGregorianCalendar.setDay(gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH));
		} catch (DatatypeConfigurationException e) {
		    e.printStackTrace();
		}
		return xMLGregorianCalendar;
	}
	
	public static void addErrorMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
	}
	
	public static void addInfoMessage(String message) {
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
	}
	
	 public static String getRealPathArquivo() {
	        return FilesUtils.getRealPathArquivo();
	    }

	    /**
	     * Pega o caminho real.
	     * @param caminho caminho
	     * @return caminho real
	     */
	    public static String getPath(String caminho) {
	        return getRealPath("").replaceAll(getServletContext().getContextPath().replaceAll(
	                Constantes.SEPARATOR, ""), caminho);
	    }

	    /**
	     * Pega um parametro do request.
	     * @param nome nome do parametro
	     * @return parametro do request
	     */
	    public static String getParametro(String nome) {
	        return getHttpServletRequest().getParameter(nome);
	    }

	    /**
	     * Pega um parametro do request.
	     * @param nome nome do parametro
	     * @return parametro do request
	     */
	    public static Object getAtributo(String nome) {
	        return getHttpServletRequest().getAttribute(nome);
	    }

	    /**
	     * Pega o path real do arquivo.
	     * @param fileName arquivo a ser localizado
	     * @return path real
	     */
	    public static String getRealPath(String fileName) {
	        return FilesUtils.getRealPath(fileName);
	    }

	    public static String getContextPath() {
	        return getHttpServletRequest().getContextPath();
	    }

	    public static FacesContext getContext() {
	        return FacesContext.getCurrentInstance();
	    }

	    public static Map<String, Object> getRequestMap() {
	        return getRequestMap(getContext());
	    }

	    public static Map<String, Object> getRequestMap(FacesContext context) {
	        return getExternalContext(context).getRequestMap();
	    }

	    public static ExternalContext getExternalContext() {
	        return getExternalContext(getContext());
	    }

	    public static ExternalContext getExternalContext(FacesContext context) {
	        return context.getExternalContext();
	    }

	    public static HttpServletRequest getHttpServletRequest() {
	        return getHttpServletRequest(getContext());
	    }

	    public static HttpServletRequest getHttpServletRequest(FacesContext context) {
	        return (HttpServletRequest) getExternalContext(context).getRequest();
	    }

	    public static HttpServletResponse getHttpServletResponse() {
	        return getHttpServletResponse(getContext());
	    }

	    public static HttpServletResponse getHttpServletResponse(FacesContext context) {
	        return (HttpServletResponse) getExternalContext(context).getResponse();
	    }

	    public static ServletContext getServletContext() {
	        return getServletContext(getContext());
	    }

	    public static ServletContext getServletContext(FacesContext context) {
	        return (ServletContext) getExternalContext(context).getContext();
	    }

	    public static HttpSession getSession() {
	        return getSession(getContext());
	    }

	    public static HttpSession getSession(FacesContext context) {
	        return (HttpSession) getExternalContext(context).getSession(true);
	    }
}