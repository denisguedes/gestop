package br.com.optimized.config.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class AntiSamyFilter implements Filter {

	private String POLICY_FILE;
	
	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(new RequestAntiSamyWrapper((HttpServletRequest) request, POLICY_FILE), response);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		POLICY_FILE = filterConfig.getInitParameter("POLICY_FILE");
	}

}
