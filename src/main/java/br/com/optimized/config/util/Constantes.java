/**
 * 
 */
package br.com.optimized.config.util;

/**
 * @author Denis Guedes
 * @Sice 23.02.2017
 *
 */
public class Constantes {
	
    public static final String ARQUIVO = "arquivo";

    public static final String SEPARATOR = "/";

    public static final String USUARIO_LOGADO_DO_SISTEMA = "USUARIO_LOGADO_NO_SISTEMA";

    public static final String MENSAGEM_ERRO_GERAL = "MENSAGEM_ERRO_GERAL";
	
	public static final String USUARIO_ATIVO = "ATIVO";
	
	public static final String USUARIO_INATIVO = "INATIVO";
	
	public static final String EMAIL_PATTERN = 
	    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static final String ANO = "Ano";
	
	public static final String JANEIRO = "Janeiro";
	public static final String FEVEREIRO = "Fevereiro";
	public static final String MARCO = "Março";
	public static final String ABRIL = "Abril";
	public static final String MAIO = "Maio";
	public static final String JUNHO = "Junho";
	public static final String JULHO = "Julho";
	public static final String AGOSTO = "Agosto";
	public static final String SETEMBRO = "Setembro";
	public static final String OUTUBRO = "Outubro";
	public static final String NOVEMBRO = "Novembro";
	public static final String DEZEMBRO = "Dezembro";
	
	public static final String PERIODO_MENSAL = "Mensal";
	public static final String PERIODO_BIMESTRAL = "Bimestral";
	public static final String PERIODO_TRIMESTRAL = "Trimestral";
	public static final String PERIODO_SEMESTRAL = "Semestral";
	public static final String PERIODO_ANUAL = "Anual";
	
	public static final String PRIMEIRO_BIMESTRE = "1º Bimestre";
	public static final String SEGUNDO_BIMESTRE = "2º Bimestre";
	public static final String TERCEIRO_BIMESTRE = "3º Bimestre";
	public static final String QUARTO_BIMESTRE = "4º Bimestre";
	public static final String QUINTO_BIMESTRE = "5º Bimestre";
	public static final String SEXTO_BIMESTRE = "6º Bimestre";
	
	public static final String PRIMEIRO_TRIMESTRE = "1º Trimestre";
	public static final String SEGUNDO_TRIMESTRE = "2º Trimestre";
	public static final String TERCEIRO_TRIMESTRE = "3º Trimestre";
	public static final String QUARTO_TRIMESTRE = "4º Trimestre";
	
	public static final String PRIMEIRO_SEMESTRE = "1º Semestre";
	public static final String SEGUNDO_SEMESTRE = "2º Semestre";
	
	/*Estados*/
	public static final String AC = "AC";
	public static final String AL = "AL";
	public static final String AP = "AP";
	public static final String AM = "AM";
	public static final String BA = "BA";
	public static final String CE = "CE";
	public static final String ES = "ES";
	public static final String GO = "GO";
	public static final String MA = "MA";
	public static final String MT = "MT";
	public static final String MS = "MS";
	public static final String MG = "MG";
	public static final String PA = "PA";
	public static final String PB = "PB";
	public static final String PR = "PR";
	public static final String PE = "PE";
	public static final String PI = "PI";
	public static final String RJ = "RJ";
	public static final String RN = "RN";
	public static final String RS = "RS";
	public static final String RO = "RO";
	public static final String RR = "RR";
	public static final String SC = "SC";
	public static final String SP = "SP";
	public static final String SE = "SE";
	public static final String TO = "TO";
	public static final String DF = "DF";
	
	/*Verbas*/
	public static final String VERBA_ANIVERSARIO = "Aniversário";
	public static final String VERBA_INAUGURACAO = "Inauguração";
	public static final String VERBA_REINAUGURACAO = "Reinauguração";
	public static final String VERBA_PD_NET_EDI = "Pd@net/EDI";
	public static final String VERBA_LOGISTICA = "Logística";
	public static final String VERBA_FIDELIDADE = "Fidelidade";
	public static final String VERBA_MENSAL = "Mensal";
	public static final String VERBA_CRESCIMENTO = "Crescimento";
	public static final String VERBA_ATINGIMENTO_META = "Atingimento de Meta";
	
	/*Siglas das verbas*/
	public static final String SIGLA_VERBA_ANIVERSARIO = "AN";
	public static final String SIGLA_VERBA_INAUGURACAO = "IN";
	public static final String SIGLA_VERBA_REINAUGURACAO = "RN";
	public static final String SIGLA_VERBA_PD_NET_EDI = "PD";
	public static final String SIGLA_VERBA_LOGISTICA = "LO";
	public static final String SIGLA_VERBA_FIDELIDADE = "FI";
	public static final String SIGLA_VERBA_MENSAL = "ME";
	public static final String SIGLA_VERBA_CRESCIMENTO = "CR";
	public static final String SIGLA_VERBA_ATINGIMENTO_META = "AM";
	
	/*Tipo de verba*/
	public static final String TIPO_VERBA_EVENTUAL = "Eventual";
	public static final String TIPO_VERBA_MENSAL = "Mensal";
	public static final String TIPO_VERBA_CRESCIMENTO = "Crescimento";
	public static final String TIPO_VERBA_ATINGIMENTO_META = "Atingimento de Meta";
	public static final String TIPO_VERBA_EXTRA = "Extra";
	
	/*Sigla tipo verba*/
	public static final String SIGLA_TIPO_VERBA_EVENTUAL = "ve";
	public static final String SIGLA_TIPO_VERBA_MENSAL = "vm";
	public static final String SIGLA_TIPO_VERBA_CRESCIMENTO = "vc";
	public static final String SIGLA_TIPO_VERBA_ATINGIMENTO_META = "vam";
	public static final String SIGLA_TIPO_VERBA_EXTRA = "vex";
	
	/*Forma de avaliação opção verba*/
	public static final String VALOR_FIXO_OPCAO_VERBA = "Valor Fixo";
	public static final String PERCENTUAL_OPCAO_VERBA = "Percentual";
	
	/*Entidade Contrato*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_NOME_CONTRATO = "O nome do contrato deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NOME_CONTRATO = "O nome do contrato deve ter no máximo 25 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NUMERO_CONTRATO = "O número do contrato deve ter no máximo 10 caracteres.";
	public static final String CAMPO_STATUS_ATIVO = "Ativo";
	public static final String CAMPO_STATUS_INATIVO = "Inativo";
	public static final String CAMPO_STATUS_PENDENTE_AUTORIZACAO = "Pendente de autorização";
	public static final String CAMPO_STATUS_RASCUNHO = "Rascunho";
	public static final String CAMPO_RENOVACAO_SIM = "Sim";
	public static final String CAMPO_RENOVACAO_NAO = "Não";
	public static final String CAMPO_ABATIMENTO_SIM = "Sim";
	public static final String CAMPO_ABATIMENTO_NAO = "Não";
	public static final String CAMPO_STATUS_REJEITADO = "Rejeitado";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_INFORMACOES = "O campo de informações deve ter no máximo 1000 caracteres.";
	
	/*Entidade Titulo*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NUMERO_TITULO = "O número do título deve ter no máximo 25 caracteres.";
	
	/*Entidade verba*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO = "A Verba deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO = "O número do título deve ter no máximo 25 caracteres.";
	
	/*Entidade VerbaExtra*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_TIPO_VERBA = "Nome da verba extra deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_TIPO_VERBA = "Nome da verba extra deve ter no máximo 25 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_INFORMACOES_VERBA_EXTRA = "O campo de informações deve ter no máximo 1000 caracteres.";
	public static final String CAMPO_STATUS_PENDENTE_AUTORIZACAO_VERBA_EXTRA = "Pendente de autorização";
	public static final String CAMPO_STATUS_PE_VERBA_EXTRA = "P";
	public static final String CAMPO_PAGO_VERBA_EXTRA = "Pago";
	public static final String CAMPO_PA_VERBA_EXTRA = "G";
	public static final String CAMPO_AUTORIZADO_VERBA_EXTRA = "Autorizado";
	public static final String CAMPO_AT_VERBA_EXTRA = "A";
	public static final String CAMPO_REJEITADO_VERBA_EXTRA = "Rejeitado";
	public static final String CAMPO_REJ_VERBA_EXTRA = "R";
	
	public static final String CAMPO_STATUS_PENDENTE_AUTORIZACAO_VERBA_EVENTUAL = "Pendente de autorização";
	public static final String CAMPO_STATUS_PE_VERBA_EVENTUAL = "P";
	public static final String CAMPO_PAGO_VERBA_EVENTUAL = "Pago";
	public static final String CAMPO_PA_VERBA_EVENTUAL = "G";
	public static final String CAMPO_AUTORIZADO_VERBA_EVENTUAL = "Autorizado";
	public static final String CAMPO_AT_VERBA_EVENTUAL = "A";
	public static final String CAMPO_REJEITADO_VERBA_EVENTUAL = "Rejeitado";
	public static final String CAMPO_REJ_VERBA_EVENTUAL = "R";
	
	/*Entidade TipoVerba*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_VERBA_EXTRA = "O tipo da Verba deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_VERBA_EXTRA = "O tipo da Verba deve ter no máximo 40 caracteres.";
	
	/*Entidade TipoPagamento*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_TIPO_PAGAMENTO = "O tipo do pagamento deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_TIPO_PAGAMENTO = "O tipo do pagamento deve ter no máximo 25 caracteres.";
	
	/*Entidade ProdutoEmpresa*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_PRODUTO_EMPRESA = "O nome do produto deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_PRODUTO_EMPRESA = "O nome do produto deve ter no máximo 100 caracteres.";
	
	/*Entidade Acordo*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_ACORDO = "O acordo deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_ACORDO = "O acordo deve ter no máximo 25 caracteres.";
	
	/*Entidade Cidade*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_CIDADE_CIDADE = "A cidade deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_CIDADE_CIDADE = "A cidade deve ter no máximo 60 caracteres.";
	
	/*Entidade Contato*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NOME_CONTATO = "O nome do contato deve ter no máximo 80 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_EMAIL_CONTATO = "O email do contato deve ter no máximo 60 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_TELEFONE_CONTATO = "O telefone do contato deve ter no máximo 20 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_FUNCAO_CONTATO = "A função do contato deve ter no máximo 25 caracteres.";
	
	/*Entidade Período*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_PERIODO = "O Período deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_PERIODO = "O Período deve ter no máximo 25 caracteres.";
	
	/*Entidade NotaFiscal*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NUMERO_NF = "O número da nota fiscal deve ter no máximo 15 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_SERIE_NF = "A série da nota fiscal deve ter no máximo 15 caracteres.";
	
	/*Entidade ItemNotaFiscal*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_EMPRESA_CLIENTE_TIPO = "O tipo da empresa cliente deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_EMPRESA_CLIENTE_TIPO = "O tipo da empresa cliente deve ter no máximo 15 caracteres.";
	
	/*Entidade Indexador*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_INDEXADOR = "Nome do indexador deve ter no máximo 60 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_SIGLA_INDEXADOR = "A sigla do indexador deve ter no máximo 10 caracteres.";
	
	/*Entidade ImportacaoTitulo*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NUMERO_TITULO_IMPORTACAO_TITULO = "O número do título deve ter no máximo 25 caracteres.";
	
	/*Entidade ImportacaoProdutoEmpresa*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_CODIGO_IMPORTACAO_PRODUTO_EMPRESA = "O código do produto deve ter no máximo 10 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_IMPORTACAO_PRODUTO_EMPRESA = "O nome do produto deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_IMPORTACAO_PRODUTO_EMPRESA = "O nome do produto deve ter no máximo 100 caracteres.";
	
	/*Entidade ImportacaoNotaFiscal*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NUMERO_NF_IMPORTACAO_NF = "O número da nota fiscal deve ter no máximo 15 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_SERIE_NF_IMPORTACAO_NF = "A série da nota fiscal deve ter no máximo 15 caracteres.";
	
	/*Entidade ImportacaoItemNF*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_EMPRESA_CLIENTE_TIPO_IMPORTACAO_ITEM_NF = "O tipo da empresa cliente deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_EMPRESA_CLIENTE_TIPO_IMPORTACAO_ITEM_NF = "O tipo da empresa cliente deve ter no máximo 15 caracteres.";
	
	/*Entidade ImportacaoEndereco*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_LOGRADOURO_IMPORTACAO_ENDERECO = "O logradouro deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_LOGRADOURO_IMPORTACAO_ENDERECO = "O logradouro deve ter no máximo 80 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_COMPLEMENTO_IMPORTACAO_ENDERECO = "O complemento do endereço deve ter no máximo 80 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_BAIRRO_IMPORTACAO_ENDERECO = "O bairro do endereço deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_BAIRRO_IMPORTACAO_ENDERECO = "O bairro do endereço deve ter no máximo 80 caracteres.";
	
	/*Entidade ImportacaoEmpresaCliente*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_RAZAO_SOCIAL_IMPORTACAO_EMPRESA_CLIENTE = "A razão social da empresa cliente deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_RAZAO_SOCIAL_IMPORTACAO_EMPRESA_CLIENTE = "A razão social da empresa cliente deve ter no máximo 100 caracteres.";
	
	/*Entidade GrupoResponsavel*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_GRUPO_RESPONSAVEL = "O nome do grupo responsável deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_GRUPO_RESPONSAVEL = "O nome do grupo responsável deve ter no máximo 40 caracteres.";
	
	/*Entidade Estado*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_ESTADO_ESTADO = "O estado deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_ESTADO_ESTADO = "O estado deve ter no máximo 60 caracteres.";
	
	/*Entidade EscolhaVerba*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_ESCOLHA_VERBA = "O nome da regra da verba deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_ESCOLHA_VERBA = "O nome da regra da verba deve ter no máximo 25 caracteres.";
	
	/*Entidade Endereco*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_LOGRADOURO_ENDERECO = "O Logradouro deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_LOGRADOURO_ENDERECO = "O Logradouro deve ter no máximo 200 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_COMPLEMENTO_ENDERECO = "O Complemento deve ter no máximo 80 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_BAIRRO_ENDERECO = "O Bairro deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_BAIRRO_ENDERECO = "O Bairro deve ter no máximo 80 caracteres.";
	
	/*Entidade EmpresaCliente*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_RAZAO_SOCIAL_EMPRESA_CLIENTE = "A razão social da empresa cliente deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_RAZAO_SOCIAL_EMPRESA_CLIENTE = "A razão social da empresa cliente deve ter no máximo 200 caracteres.";
	
	/*Entidade TipoClassificacaoLoja */
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_TIPO_CLASSIFICACAO_LOJA = "O tipo de classificação de loja deve ter no mínimo 1 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_TIPO_CLASSIFICACAO_LOJA = "O tipo de classificação de loja ter no máximo 40 caracteres.";
	
	/*Entidade Empresa*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_CODIGO_EMPRESA = "O código da empresa deve ter no máximo 10 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_NOME_FANTASIA_EMPRESA = "O Nome Fantasia deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_NOME_FANTASIA_EMPRESA = "O Nome Fantasia deve ter no máximo 120 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_RAZAO_SOCIAL_EMPRESA = "A razão social da empresa cliente deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_RAZAO_SOCIAL_EMPRESA = "A razão social da empresa cliente deve ter no máximo 120 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_INSCRICAO_ESTADUAL_EMPRESA = "A Inscrição Estadual deve ter no máximo 20 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_INSCRICAO_MUNICIPAL_EMPRESA = "A Inscrição Municipal deve ter no máximo 20 caracteres.";
	
	/*Entidade Conciliacao */
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_CONCILIACAO = "A descrição da conciliação deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_CONCILIACAO = "A descrição da conciliação deve ter no máximo 25 caracteres.";
	
	/*Entidade VerbaPagCrescimento */
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_VERBA_PAG_CRESCIMENTO = "O nome da regra da verba deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_VERBA_PAG_CRESCIMENTO = "O nome da regra da verba deve ter no máximo 25 caracteres.";
	
	/*Entidade usuário*/
	public static final String MSG_FEEDBACK_USUARIO_CPF_INVALIDO = "CPF Inválido.";
	public static final String MSG_FEEDBACK_USUARIO_EMAIL_INVALIDO = "Email Inválido.";
	
	/*Entidade VerbaEventual*/
	public static final String MSG_FEEDBACK_USUARIO_TAM_MAX_INFO_VERBA__LANCAMENTO_EVENTUAL = "O campo de informações deve ter no máximo 1000 caracteres.";
	
	
	/*Entidade Email SMTP*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_HOST_SMTP = "O host do servidor SMTP deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_HOST_SMTP = "O host do servidor SMTP deve ter no máximo 40 caracteres.";
	
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_EMAIL_SMTP = "O e-mail do servidor SMTP deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_EMAIL_SMTP = "O e-mail do servidor SMTP deve ter no máximo 120 caracteres.";
	
	
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_USUARIO_EMAIL_SMTP = "O usuário do e-mail SMTP deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_USUARIO_EMAIL_SMTP = "O usuário do e-mail SMTP deve ter no máximo 120 caracteres.";
	
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_SENHA_EMAIL_SMTP = "A senha do e-mail SMTP deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_SENHA_EMAIL_SMTP = "A senha do e-mail SMTP deve ter no máximo 20 caracteres.";
	

	/*Entidade AgrupadorEmpresa*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_AGRUPADOR_EMPRESA = "A descrição do agrupador da empresa cliente deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_AGRUPADOR_EMPRESA = "A descrição do agrupador da empresa cliente deve ter no máximo 60 caracteres.";
	
	/*Entidade CategoriaVerbaExtra*/
	public static final String MSG_FEEDBACK_TAMANHO_MIN_DESCRICAO_CATEGORIA_VERBA_EXTRA = "A descrição da categoria deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_TAMANHO_MAX_DESCRICAO_CATEGORIA_VERBA_EXTRA = "A descrição da categoria deve ter no máximo 80 caracteres.";
	
	/*Entidade AgrupadorProduto*/
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MIN_DESCRICAO_AGRUPADOR_PRODUTO = "A descrição do agrupador do produto deve ter no mínimo 3 caracteres.";
	public static final String MSG_FEEDBACK_USUARIO_TAMANHO_MAX_DESCRICAO_AGRUPADOR_PRODUTO = "A descrição do agrupador do produto deve ter no máximo 60 caracteres.";
	
	/*Conciliação*/
	public static final int CONCILIACAO_DETALHE_NOTAS_FISCAIS = 0;
	public static final int CONCILIACAO_DETALHE_VERBAS_MENSAIS = 1;
	public static final int CONCILIACAO_DETALHE_VERBAS_CRESCIMENTO = 2;
	public static final int CONCILIACAO_DETALHE_VERBAS_ATING_META = 3;
	public static final int CONCILIACAO_DETALHE_VERBAS_EXTRAS = 4;
	public static final int CONCILIACAO_DETALHE_VERBAS_EVENTUAIS = 5;
	
	
}
