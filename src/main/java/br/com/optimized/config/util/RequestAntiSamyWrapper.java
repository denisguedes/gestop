package br.com.optimized.config.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;

public class RequestAntiSamyWrapper extends HttpServletRequestWrapper{

	private final transient String POLICY_FILE;
	
	public RequestAntiSamyWrapper(HttpServletRequest request, String policyFile) {
		super(request);
		this.POLICY_FILE = policyFile;
	}
	
	private String cleanXSS(String value) {
		Policy policy = null;
		CleanResults cleanResults = null;
		try {
			policy = Policy.getInstance(RequestAntiSamyWrapper.class.getResourceAsStream(POLICY_FILE));
			AntiSamy sanitizer = new AntiSamy(policy);
			cleanResults = sanitizer.scan(value);
			return cleanResults.getCleanHTML();
		} catch (Exception e) {
			e.printStackTrace();
			return value;
		} 
	}

	@Override
	public String getHeader(String name) {
		String value = super.getHeader(name);
		if(value == null) {
			return null;
		}
		return cleanXSS(value);
	}

	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);
		if(value == null) {
			return null;
		}
		return cleanXSS(value);
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] values = super.getParameterValues(name);
		if(values == null) {
			return null;
		}
		int count  = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++) {
			encodedValues[i] = cleanXSS(values[i]);
		}
		return encodedValues;
	}

	
}
