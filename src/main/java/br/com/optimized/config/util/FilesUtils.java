package br.com.optimized.config.util;

import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.hibernate.internal.util.StringHelper;

/**
 * Classe responsável por fazer transformações e formatações em números decimais.
 * @author heber
 */
public final class FilesUtils {

    /** SIMPLE_DATE_FORMAT. */
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    private static final DecimalFormat FORMATTER = new DecimalFormat(
            "###,###,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));

    /** Tamanho em Kilobytes. */
    private static final Long KILO = 1000L;
    /** Tamanho em Megabytes. */
    private static final Long MEGA = 1000L * KILO;
    /** Tamanho em Gigabytes. */
    private static final Long GIGA = 1000L * MEGA;

    /**
     * Privatização do construtor.
     */
    private FilesUtils() { }

    /**
     * Salva um arquivo texto.
     * @param destino arquivo destino
     * @param conteudo conteudo do arquivo
     * @throws IOException erros de escrita
     */
    public static void salvar(String destino, String conteudo) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(destino));
        out.write(conteudo);
        out.flush();
        out.close();
    }

    /**
     * Pega o tamanho do arquivo/pasta em disco.
     * @param file arquivo que sera lido
     * @return tamanho em bytes
     */
    public static long getSize(String file) {
        return getSize(getFile(file));
    }

    /**
     * Pega o tamanho do arquivo/pasta em disco.
     * @param file arquivo que sera lido
     * @return tamanho em bytes
     */
    public static long getSize(File file) {
        long size = 0;

        if (file == null) {
            return size;
        }

        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return file.length();
        }

        for (File f : listFiles) {
            if (f.isDirectory()) {
                size += getSize(f);
            } else {
                size += f.length();
            }
        }
        return size + file.length();
    }

    /**
     * Pega um arquivo, existente ou nao.
     * @param relativePath caminho relativo do arquivo
     * @return arquivo que sera lido
     */
    public static File getFile(String relativePath) {
        return new File(relativePath.startsWith("/arquivo")
                ? getRealPathEmpty() + Constantes.SEPARATOR + relativePath : relativePath);
    }

    /**
     * Verifica se um arquivo existe.
     * @param relativePath caminho relativo do arquivo
     * @return true se sim
     */
    public static boolean exists(String relativePath) {
        return getFile(relativePath).exists();
    }

    /**
     * Apaga um arquivo existente.
     * @param relativePath caminho relativo do arquivo
     * @return true se o arquivo foi deletado
     */
    public static boolean delete(String relativePath) {
        if (exists(relativePath)) {
            return getFile(relativePath).delete();
        }
        return false;
    }

    /**
     * Faz uma copia do arquivo.
     * @param origem arquivo origem
     * @param destino onde sera copiado
     */
    public static void copy(String origem, String destino) {
        copy(getFile(origem), getFile(destino));
    }

    /**
     * Faz uma copia do arquivo.
     * @param origem arquivo origem
     * @param destino onde sera copiado
     */
    public static void copy(File origem, File destino) {
        try {
            if (!exists(destino.getParent())) {
                new File(destino.getParent()).mkdirs();
            }
            Files.copy(origem.toPath(), new FileOutputStream(destino));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Pega o realpath do servidor sem nenhum contexto.
     * @return
     */
    public static String getRealPathEmpty() {
        return getRealPath("/");
    }

    public static String getRealPath(String fileName) {
        return getServletContext().getRealPath(fileName);
    }

    public static String getRealPathArquivo() {
        return getRealPathEmpty() + "/arquivo";
    }

    /**
     * Pega o tamanho do arquivo.
     * @param value
     * @return
     */
    public static String getSize(Number value) {

        if (value == null) {
            return "-";
        }

        double size = value.doubleValue();

        if (size >= GIGA - (200 * MEGA)) {
            return format(size / GIGA) + " Gb";
        }

        if (size >= MEGA - (300 * KILO)) {
            return format(size / MEGA) + " Mb";
        }

        if (size >= KILO - 400) {
            return format(size / KILO) + " Kb";
        }

        return format(size) + " b";
    }

    /**
     * Pega a extensao do arquivo.
     * @param fileName nome do arquivo
     * @return extensao encontrada
     */
    public static String getExtension(String fileName) {
        if (StringHelper.isEmpty(fileName) || !fileName.contains(".")) {
            return "";
        }
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    /**
     * Gera um nome de arquivo.
     * @param originalFileName nome original
     * @return novo nome de arquivo
     */
    public static String getNewFileName(String originalFileName) {
        return SIMPLE_DATE_FORMAT.format(new Date()) + System.currentTimeMillis();
    }

    /**
     * Salva uma imagem no disco e cria a miniatura.
     * @param contents bytes da imagem
     * @param diretorio onde as imagens serao salvas
     * @param name nome do arquivo
     * @param extension tipo da imagem
     * @throws IllegalArgumentException formato dos dados incorretos
     * @throws ImagingOpException erro no tratamento da imagem
     * @throws IOException erros de escrita
     */
    public static void salvarImagem(byte[] contents, File diretorio, String name, String extension)
            throws IllegalArgumentException, ImagingOpException, IOException {

        salvarImagem(ImageIO.read(new ByteArrayInputStream(contents)), diretorio, name, extension);
    }

    /**
     * Salva uma imagem no disco e cria a miniatura.
     * @param imagem imagem original
     * @param diretorio onde as imagens serao salvas
     * @param name nome do arquivo
     * @param extension tipo da imagem
     * @throws IllegalArgumentException formato dos dados incorretos
     * @throws ImagingOpException erro no tratamento da imagem
     * @throws IOException erros de escrita
     */
    public static void salvarArquivo(byte[] contents, File diretorio, String fileName)
            throws IllegalArgumentException, ImagingOpException, IOException {

        if (contents == null) {
            return;
        }

        if (!diretorio.exists()) {
            diretorio.mkdirs();
        }

        FileOutputStream out = new FileOutputStream(new File(diretorio.toString() + "/" + fileName));
        out.write(contents);
        out.flush();
        out.close();
    }

    /**
     * Salva uma imagem no disco e cria a miniatura.
     * @param imagem imagem original
     * @param diretorio onde as imagens serao salvas
     * @param name nome do arquivo
     * @param extension tipo da imagem
     * @throws IllegalArgumentException formato dos dados incorretos
     * @throws ImagingOpException erro no tratamento da imagem
     * @throws IOException erros de escrita
     */
    public static void salvarImagem(BufferedImage imagem, File diretorio, String name, String extension)
            throws IllegalArgumentException, ImagingOpException, IOException {

        if (imagem == null) {
            return;
        }

        if (!diretorio.exists()) {
            diretorio.mkdirs();
        }

        String fileName = name + "." + extension;

        ImageIO.write(imagem, extension, new File(diretorio.toString() + "/" + fileName));
    }

    /**
     * Formata o numero.
     * @param value numero que sera formatado
     * @return valor formatado
     */
    private static String format(double value) {
        return FORMATTER.format(value);
    }

    protected static ServletContext getServletContext() {
        return FacesUtil.getServletContext();
    }
}
